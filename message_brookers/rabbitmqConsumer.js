#!/usr/bin/env node

const amqp = require('amqplib/callback_api');

const path = require('path');
const dotenv = require('dotenv').config({ path: path.join(__dirname, '.env') });

const DB = require('../models/db');

const _ = require('lodash');

const moment = require('moment');

const ip = require('ip');

const msgs = {
  EMAIL_NEW_INVOICES: 'EMAIL-NEW-INVOICES',
  EMAIL_PAYMENT_RECEIPT: 'EMAIL_PAYMENT_RECEIPT',
  EMAIL_BALANCE_SUMMARIES: 'EMAIL_BALANCE_SUMMARIES',
  SMS_BALANCE_SUMMARIES: 'SMS_BALANCE_SUMMARIES'
}

amqp.connect({ protocol: 'amqp', hostname: 'localhost', port: 5672, username: 'guest', password: 'guest', vhost: '/' } , (err, conn) => {
  conn.createChannel((err, ch) => {
      if(err)
        throw err;
    const q = 'defaultQueue';

    ch.assertQueue(q, {durable: false});
    console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q);
    ch.consume(q, (msg) => {
      msg = JSON.parse(msg.content.toString());
      console.log(" [x] Received %s", msg.msg);
      switch (msg.msg) {
        case msgs.EMAIL_NEW_INVOICES:
          {
            setTimeout(() => {
              const query = 'CALL `get_newly_created_invoices_to_email`(?,?);';
              DB.exec(query, [msg.appUser.pid, 'Not Paid']).then((newInvoices) => {
                console.log(' [x] Starting to email invoices...');
                const invoice = require('../models/invoice.model');
                newInvoices.forEach((newInvoice) => {
                  invoice.getInvoiceDetails({ id: newInvoice.id }).then((invoice) => {
                    invoice = invoice.invoiceDetails.data;
                    const { docHeader, docTotals } = invoice.docSummary;
                    const tenant = require('../models/tenant.model');
                    const _ = require('lodash');
                    const moment = require('moment');
                    const numeral = require('numeral');

                    tenant.get({ id: docHeader.tenantId }).then((result) => {
                      
                      const tenant = _.first(result.tenants.data);
                      let response = {
                          message: 'Invoice sent',
                          sent:true,
                      };

                      if (tenant.contactEmail) {
                        const qrCode = require('../utils/qrcodes').generateQrCode(docHeader.docRef);
                        const { lineItems } = invoice;

                        let emailData = {
                            email: tenant.contactEmail,
                            subject: `${tenant.buildingName}, House ${tenant.houseNo} - ${docHeader.docMonth} ${_.startCase(docHeader.docType)}, Ref: ${docHeader.docRef}`,
                            body: undefined,
                            attachments: []
                        }

                        const lines = lineItems.map((lineItem, i) => {
                            return (
                                `<tr style="border-bottom: #a5a5a5 1px solid; background-color:${i%2===0?'#fffff;':'#e5e5e5;'}">
                                    <td style="width:125px;">${lineItem.chargeOn}</td>
                                    <td style="width:50px; text-align:center;">${lineItem.unitOfMeasure}</td>
                                    <td style="width:100px; text-align:right;">${lineItem.quantity}</td>
                                    <td style="width:110px; text-align:right;">${numeral(lineItem.unitCost).format('0,0')}</td>
                                    <td style="width:110px; text-align:right;">${numeral(lineItem.amount).format('0,0')}</td>
                                </tr>`
                            );
                        }).join('');

                        emailData.body = (`<div style="border:#e5e5e5 2px solid;padding:5px;height:610px; width:550px;">
                        <div style="text-align:center">
                          <strong>INVOICE</strong>
                        </div>
                        <div>
                          <hr style="border:#e5e5e5 1px solid;" />
                        </div>
                        <div style="height:150px;width:inherit; clear:both;">
                          <div style="width:100px; float:left;">
                            <img style="width:100px;" src="http://${ip.address()}:81/qrcodes/${qrCode}" alt="${docHeader.docRef}" />
                          </div>
                          <div style="width:200px; float:left; text-align:center;">
                            <!-- Will have a paid stamp if invoice is paid in full -->
                          </div>
                          <div style="float:right; width:220px;">
                            <div style="clear:both;">
                              <div style="float:left;">
                                <div style="float:left;">
                                  <div style="padding:5px;">
                                    From
                                  </div>
                                </div>
                              </div>
                              <div style="float:left; border-left:#e5e5e5 2px solid;">
                                <div style="padding:5px;">
                                  <div>
                                    <strong>${msg.appUser.name}</strong>
                                  </div>
                                  <div>
                                    ${tenant.buildingName}
                                  </div>
                                  <div>
                                    ${tenant.buildingLocation}
                                  </div>                                  
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div style="height:100px;width:inherit; clear:both;">
                          <div style="width:300px; float:left;">
                            <div style="clear:left;">
                              <div style="float:left; border-right:#e5e5e5 2px solid;">
                                <div style="padding:5px;">
                                  <div>
                                    Invoice Ref
                                  </div>
                                  <div>
                                    Issue Date
                                  </div>
                                  <div>
                                    Subject
                                  </div>
                                </div>
                              </div>
                              <div style="float:left;">
                                <div style="padding:5px;">
                                  <div>
                                    ${docHeader.docRef}
                                  </div>
                                  <div>
                                    ${docHeader.docDate}
                                  </div>
                                  <div>
                                    ${docHeader.docMonth} <span style="text-transform: capitalize;">${docHeader.docType}</span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div style="float:right; width:220px;">
                            <div style="clear:both;">
                              <div style="float:left;">
                                <div style="float:left;">
                                  <div style="padding:5px; width:30px">
                                    For
                                  </div>
                                </div>
                              </div>
                              <div style="float:left; border-left:#e5e5e5 2px solid;">
                                <div style="padding:10px;">
                                  <div>
                                    <strong>${tenant.tenantName}</strong>
                                  </div>
                                  <div>
                                    ${tenant.buildingName}
                                  </div>
                                  <div>
                                    ${tenant.houseNo}
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div style="height:300px;width:inherit; clear:both;">
                          <div style="padding:5px;">                            
                            <table id="line-items" style="width:100%; border-collapse: collapse; vertical-align:middle;">
                              <thead>
                                <tr style="border-bottom: #a5a5a5 1px solid; background-color:#d5f5f5; text-align:left;">
                                  <th style="width:130px;">Item</th>
                                  <th style="width:50px; text-align:center;">UOM</th>
                                  <th style="width:100px; text-align:right;">Quantity</th>
                                  <th style="width:110px; text-align:right;">Unit Price</th>
                                  <th style="width:110px; text-align:right;">Line Total</th>
                                </tr>
                              </thead>
                              <tbody>
                                ${lines}
                                <!-- after the loop -->
                                <tr>
                                  <td colspan="5" style="text-align:right; border:none;">
                                    <div style="float:right; width:220px;">
                                      <table style="width:100%; border-collapse: collapse; vertical-align:middle;">
                                        <tbody>
                                          <tr style="border-bottom: #a5a5a5 1px solid; text-align:right; vertical-align-top;">
                                            <td style="width:100px;">Sub Total</span></td>
                                            <td>${numeral(docTotals.grandTotal).format('0,0')}</td>
                                          </tr>
                                          <tr style="border-bottom: #a5a5a5 1px solid; text-align:right; vertical-align-top;">
                                            <td style="width:100px;">Amount Paid</span></td>
                                            <td>${numeral(docTotals.paidToDate).format('0,0')}</td>
                                          </tr>
                                          <tr style="border-bottom: #a5a5a5 1px solid; text-align:right; vertical-align-top;">
                                            <td style="width:100px;">Amount Due</span></td>
                                            <td>${numeral(docTotals.totalBalance).format('0,0')}</td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <div style="height:50px;width:inherit; clear:both;">
                        <div>
                          <i>Powered by <a style="text-decoration:none;" href="https://renter.co.ke">RenterKe</a></i>
                        </div>
                        </div>
                      </div>
                      `);
            
            
                      const pdf = require('html-pdf');                      
                      const options = { format: 'A4' };

                      pdf.create(emailData.body, options).toStream((err, stream) => {
                          const fs = require('fs');
                          
                          if (err) return console.log(err);

                          stream.pipe(fs.createWriteStream(`/var/www/html/pdfdocs/${docHeader.docRef}.pdf`));

                          emailData.attachments.push({
                              filename: emailData.subject,
                              path: `/var/www/html/pdfdocs/${docHeader.docRef}.pdf`,
                              contentType: 'application/pdf'
                          });

                          const notification = require('../models/notify.model');

                          notification.sendEmail(emailData.email, emailData.subject, emailData.body,emailData.attachments).then((resp) => {
                            fs.unlinkSync(`/var/www/html/pdfdocs/${docHeader.docRef}.pdf`);
                            return;
                          });
                      });
                      }
          
                    }).catch((err) => {
                      throw err;
                    });
                  }).catch((err) => {
                    throw err;
                  });
                });
              }).catch((err) => {
                throw err;
              });
            }, 60000);
            return;
          }
        case msgs.EMAIL_PAYMENT_RECEIPT:
          {
            setTimeout(() => {
              
              const { appUser, data, tenant } = msg;

              if (tenant.contactEmail) {
                const notification = require('../models/notify.model');
                const numeral = require('numeral');
                const qrCode = require('../utils/qrcodes').generateQrCode(data.paymentRef);
                      
                const subject = `${tenant.buildingName} Payment Receipt - Ref: ${data.paymentRef}`
                const receipt = `<div style="border:#e5e5e5 2px solid; height:400px; width:400px; padding:5px;">
                              <div style="text-align:center;">
                                  <strong>Payment Receipt</strong>
                              </div>
                              <div style="width: 100%; height:150px;overflow: hidden;">
                              <div style="float:left;width:198px;">
                                  <div>
                                  ${tenant.buildingName}
                                  </div>
                                  <div>
                                  Received From
                                  </div>
                                  <div>
                                  ${tenant.tenantName} - ${tenant.houseNo}
                                  </div>
                              </div>
                              <div style="float:left;width:198px; text-align:right;">
                                  <div>
                                  <img style="width:100px;" src="http://${ip.address()}:81/qrcodes/${qrCode}" alt="${data.paymentRef}" />
                                  </div>
                                  <div style="float:right; margin-top:10px;">
                                  <div style="float:left;width:40px;text-align:left;">
                                      <div>
                                      Ref:
                                      </div>
                                      <div>
                                      Date:
                                      </div>
                                  </div>
                                  <div style="float:left;width:125px; padding-left:5px; text-align:left;">
                                      <div>
                                      ${data.paymentRef}
                                      </div>
                                      <div>
                                      ${moment().format('DD-MM-YYYY')}
                                      </div>
                                  </div>
                                  </div>
                              </div>
                              </div>
                              <div>
                              <hr />
                              </div>
                              <div>
                              <div>
                                  <strong>PMT METHOD</strong>
                              </div>
                              <div>
                                  ${data.receivingAcc}
                              </div>
                              <div>
                                  Ref: ${data.transactionRef}
                              </div>
                              </div>
                              <div style="margin-top:10px;">
                              <table style="width:100%; border-collapse: collapse; vertical-align:middle;">
                                  <thead style="text-align:left;">
                                  <tr style="background-color:#d5f5f5;">
                                      <th>Particulars</th>
                                      <th style="text-align:right;">Amount</th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                  <tr>
                                      <td>Payment to ${tenant.buildingName}</td>
                                      <td style="text-align:right;">${numeral(data.amount).format('0,0.0')}</td>
                                  </tr>
                                  </tbody>
                              </table>
                              </div>
                              <div>
                              <div style="margin-top:20px;">
                                  <div style="width:195px; float:left; text-align:left;">
                                      <span>Your balance is KES <strong>${numeral(data.tenantBalance).format('0,0.0')}</strong></span>
                                  </div>
                                  <div style="width:195px; float:left; text-align:right;">
                                      <i>Received with thanks ${appUser.userFirstName}</i>
                                  </div>
                              </div>
                              </div>
                              <div style="margin-top:20px; float:left; width:inherit;">
                                  <div>
                                      <i>Powered by <a style="text-decoration:none;" href="renter.co.ke">RenterKe</a></i>
                                  </div>
                              </div>
                          </div>`;
                          
                const pdf = require('html-pdf');
                const options = { format: 'A4' };
  
                pdf.create(receipt, options).toStream((err, stream) => {
                  const fs = require('fs');

                  let attachments = [];
                          
                  if (err) return console.log(err);
      
                  stream.pipe(fs.createWriteStream(`/var/www/html/pdfdocs/${data.paymentRef}.pdf`));
      
                  attachments.push({
                    filename: subject,
                    path: `/var/www/html/pdfdocs/${data.paymentRef}.pdf`,
                    contentType: 'application/pdf'
                  });
                              
                  notification.sendEmail(tenant.contactEmail, subject, receipt, attachments).then((resp) => {
                    fs.unlinkSync(`/var/www/html/pdfdocs/${data.paymentRef}.pdf`);
                    return;
                  });
                });
              }
              
            }, 1500);
            return;
          }
        case msgs.EMAIL_BALANCE_SUMMARIES:
          {
            setTimeout(() => {
              const { appUser, data } = msg;
              const { arrearsDetails: details } = data.arrearsDetails;
              details.forEach((item) => {
                if (item.tenantEmail) {
                  let lines = '';
                  item.lineItems.forEach((line,j) => {
                    lines += `
                      <tr style="border-bottom: #a5a5a5 1px solid; background-color:${j % 2 === 0 ? '#fffff;' : '#e5e5e5;'}">
                        <td>${line.item}</td>
                        <td>${line.month}</td>
                        <td style="text-align:right;">${line.balance}</td>
                      </tr>`;
                  });

                  const qrCode = require('../utils/qrcodes').generateQrCode(`Balance_reminder_to_${item.tenantName.split(' ').join('_')}`);

                  let emailData = {
                    email: item.tenantEmail,
                    subject: `${item.buildingName}, House ${item.houseNo} - Balance Reminder`,
                    body: undefined,
                    attachments: []
                  }

                  emailData.body = (
                    `<div style="border:#e5e5e5 2px solid;padding:5px;height:610px; width:560px;">
                    <div style="text-align:center">
                      <strong>Balance Summary</strong>
                    </div>
                    <div>
                      <hr style="border:#e5e5e5 1px solid;" />
                    </div>
                    <div style="height:150px;width:inherit; clear:both;">
                      <div style="width:100px; float:left;">
                        <img style="width:100px;" src="http://${ip.address()}:81/qrcodes/${qrCode}" alt="${item.tenantName}" />
                      </div>
                      <div style="width:200px; float:left; position:absolute; left:135px; text-align:center;">
                      ${
                                    item.totalAmountDue ?
                                      ``
                                      :
                                      `<div style="color:#008202">
                          <h2>Paid In Full</h2>
                        </div>`
                                    }
                      </div>
                      <div style="float:right; position:absolute; left:330px; width:260px;">
                      <div style="clear:both;">
                        <div style="float:left;">
                        <div style="float:left;">
                          <div style="padding:5px;">
                          From
                          </div>
                        </div>
                        </div>
                        <div style="float:left; border-left:#e5e5e5 2px solid;">
                        <div style="padding:5px;">
                          <div>
                          <strong>${appUser.user.name}</strong>
                          </div>
                          <div>
                          ${item.buildingName}
                          </div>
                          <div>
                          ${item.buildingLocation}
                          </div>                                  
                        </div>
                        </div>
                      </div>
                      </div>
                    </div>
                    <div style="height:100px;width:inherit; clear:both;">
                      <div style="width:480px; float:left;">
                      <div style="clear:left;">
                        <div style="float:left; border-right:#e5e5e5 2px solid;">
                        <div style="padding:5px;">
                          <div>
                            Subject
                          </div>
                        </div>
                        </div>
                        <div style="float:left;">
                        <div style="padding:5px;">
                          <div>
                            Balance Summary
                          </div>
                        </div>
                        </div>
                      </div>
                      </div>
                      <div style="float:right; position:absolute; left:330px; width:260px;">
                      <div style="clear:both;">
                        <div style="float:left;">
                        <div style="float:left;">
                          <div style="padding:5px; width:30px">
                          For
                          </div>
                        </div>
                        </div>
                        <div style="float:left; border-left:#e5e5e5 2px solid;">
                        <div style="padding:10px;">
                          <div>
                          <strong>${item.tenantName}</strong>
                          </div>
                          <div>
                          ${item.buildingName}
                          </div>
                          <div>
                          ${item.houseNo}
                          </div>
                        </div>
                        </div>
                      </div>
                      </div>
                    </div>
                    <div style="height:300px;width:inherit; clear:both;">
                      <div style="padding:5px;">                            
                      <table id="line-items" style="width:100%; border-collapse: collapse; vertical-align:middle;">
                        <thead>
                        <tr style="border-bottom: #a5a5a5 1px solid; background-color:#d5f5f5; text-align:left;">
                          <th style="width:200px;">Item</th>
                          <th style="width:200px;">Month</th>
                          <th style="width:200px; text-align:right;">Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        ${lines}
                        <!-- after the loop -->
                        <tr>
                          <td colspan="3" style="text-align:right; border:none;">
                          <div style="float:right; width:220px;">
                            <table style="width:100%; border-collapse: collapse; vertical-align:middle;">
                            <tbody>
                              <tr style="border-bottom: #a5a5a5 1px solid; text-align:right; vertical-align-top;">
                              <td style="width:100px;">Sub Total</span></td>
                              <td>${item.totalGross}</td>
                              </tr>
                              <tr style="border-bottom: #a5a5a5 1px solid; text-align:right; vertical-align-top;">
                              <td style="width:100px;">Amount Paid</span></td>
                              <td>${item.totalPaidToDate}</td>
                              </tr>
                              <tr style="border-bottom: #a5a5a5 1px solid; text-align:right; vertical-align-top;">
                              <td style="width:100px;">Amount Due</span></td>
                              <td>${item.totalAmountDue}</td>
                              </tr>
                            </tbody>
                            </table>
                          </div>
                          </td>
                        </tr>
                        </tbody>
                      </table>
                      </div>
                    </div>
                    <div style="height:50px;width:inherit; clear:both;">
                    <div>
                      <i>Powered by <a style="text-decoration:none;" href="https://renter.co.ke">RenterKe</a></i>
                    </div>
                    </div>
                  </div>
                  `
                  );
  
                  const pdf = require('html-pdf');                      
                  const options = { format: 'A4' };
  
                  pdf.create(emailData.body, options).toStream((err, stream) => {
                      const fs = require('fs');
                      
                      if (err) return console.log(err);
  
                      stream.pipe(fs.createWriteStream(`/var/www/html/pdfdocs/${item.tenantName}.pdf`));
  
                      emailData.attachments.push({
                          filename: emailData.subject,
                          path: `/var/www/html/pdfdocs/${item.tenantName}.pdf`,
                          contentType: 'application/pdf'
                      });
  
                      const notification = require('../models/notify.model');
  
                      notification.sendEmail(emailData.email, emailData.subject, emailData.body,emailData.attachments).then((resp) => {
                        fs.unlinkSync(`/var/www/html/pdfdocs/${item.tenantName}.pdf`);
                        return;
                      });
                  });
                    
                }

                });
            },1000);
            return;
          }
        case msgs.SMS_BALANCE_SUMMARIES:
          {
            setTimeout(() => { 
              const { appUser, data } = msg;
              const { arrearsDetails: details } = data.arrearsDetails;
              const notification = require('../models/notify.model');
              const notificationCommands = require('../config/commands/notificationCommands');
              notification.checkSMSSetting(notificationCommands.BALANCE_REMINDER, appUser.user.pid).then(toSMS => {
                if (toSMS) {
                  details.forEach((item) => {
                    if (item.tenantContactPhone) {
                      const mobileNo = item.tenantContactPhone.replace('0', '+254');
                      const message = `${item.buildingName}, Balance reminder to ${item.tenantName}. Your total amount due is ${item.totalAmountDue} from ${item.totalOpenInvoices} open invoice${item.totalOpenInvoices > 1 ? `s` : ``} dating from ${item.fromMonth} to ${item.toMonth}`;
                      notification.sendSMS(item.tenantId, mobileNo, message, appUser.user.pid);
                    }
                  });
                }
            });
            }, 1000);
            return;
          }
        default:
          {
            return;
          }
      }
    }, {noAck: true});
  });
});
