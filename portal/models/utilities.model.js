const connection = require('../../config/db/mysql');
const urls = require('../../config/urls/urls');

const appUser = require('../../auth/user');

const DB = require('../../models/db');

module.exports = {
    getUtilities: () => {
        let query = "SELECT `id`,`resource`,`mode`,`unit_price` FROM `vtenantUtilities` WHERE `tenant_id` = ?";
        return new Promise((resolve, reject) => {
            DB.raw(query, [1]).then((results) => {
                return resolve(results.map(result => {
                    return {
                        utilityId: result.id,
                        name: result.resource,
                        mode: result.billed,
                        unitPrice: result.unit_price
                    }
                }));
            }).catch((err) => {
                return reject(err);
            });
        });
    },
    getUtilityReadings: (utilityId) => {
        let query = "SELECT `id`,`previous_reading`,`current_reading`,`reading_date`,`consumption`,`forMonth` FROM `vtenatUtilityReadings` WHERE `tenant_id` = ? AND utility_id = ? ORDER BY date_format(`reading_date`,'%y%m') DESC;";
        return new Promise((resolve, reject) => {
            DB.raw(query, [1, utilityId]).then((results) => {
                return resolve(results.map(result => {
                    return {
                        readingId: result.id,
                        previousReading: result.previous_reading,
                        currentReading: result.current_reading,
                        consumption: result.consumption,
                        readingDate: result.reading_date,
                        month: result.forMonth
                    }
                }));
            }).catch((err) => {
                return reject(err);
            });
        });
    }
}