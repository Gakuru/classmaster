const connection = require('../../config/db/mysql');
const urls = require('../../config/urls/urls');

const appUser = require('../../auth/user');

const DB = require('../../models/db');

module.exports = {
    get: () => {
        let query = "SELECT `consumption`,date_format(`reading_date`,'%b %Y') AS forMonth FROM `vtena" +
                "tUtilityReadings` WHERE `tenant_id` = 1 ORDER BY date_format(`reading_date`,'%y%" +
                "m') DESC LIMIT 5";
        return new Promise((resolve, reject) => {
            DB
                .raw(query, [1])
                .then((result) => {
                    return resolve({code: 200, success: true, panel: result});
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }
}