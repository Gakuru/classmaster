const appUser = require('../../auth/user');

const utilities = require('../models/utilities.model');

module.exports = {
    getUtilities: (callback) => {
        utilities.getUtilities().then((result) => {
            callback(result);
        }).catch((err) => {
            throw err;
        });
    },
    getUtilityReadings: (utilityId, callback) => {
        utilities.getUtilityReadings(utilityId).then((result) => {
            callback(result);
        }).catch((err) => {
            throw err;
        });
    }
};