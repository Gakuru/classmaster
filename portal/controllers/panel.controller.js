const appUser = require('../../auth/user');

const panel = require('../models/panel.model');

module.exports = {
    get: (callback) => {
        panel.get().then((result) => {
            callback(result);
        }).catch((err) => {
            throw err;
        });
    }
};