/*jshint esversion: 6 */

const express = require('express');
const router = express.Router();
const utulities = require('../controllers/utilities.controller');

router.get('/', (req, res) => {
    res.send('RenterKE');
});

router.get('/portal/utilities', (req, res) => {
    utulities.getUtilities((result) => {
        res.json(result);
    });
});

router.get('/portal/utilities/:uid/readings', (req, res) => {
    utulities.getUtilityReadings(req.params.uid, (result) => {
        res.json(result);
    });
});

module.exports = router;