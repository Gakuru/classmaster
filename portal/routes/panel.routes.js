/*jshint esversion: 6 */

const express = require('express');
const router = express.Router();
const panel = require('../controllers/panel.controller');

router.get('/', (req, res) => {
  res.send('RenterKE');
});

router.get('/portal/panel', (req, res) => {
  panel.get((result) => {
    res.json(result);
  });
});

module.exports = router;