module.exports = {
    respondWith401: {
        errorCode: 401,
        message: 'unauthorized request',
        fooMessage: 'Cheating Ha!'

    },
    respondWith404: {
        errorCode: 404,
        message: 'No Data',
        fooMessage: 'Cheating Ha!'
    },
    respondWithCustomMessage: message => {
        return {
            errorCode: 403,
            message: message,
            fooMessage: 'Aha, have you seen the custom message'
        }
    },
    respondToInvalidToken: {
        errorCode: 403,
        message: 'Invalid token',
        unOAuthed:true,
        fooMessage: 'Got Yah! No way through'
    }
}