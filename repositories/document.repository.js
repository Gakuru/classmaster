const connection = require('../config/db/mysql');

const DB = require('../models/db');

const moment = require('moment-timezone');

const uuid = require('uuid/v1');

const randomstring = require('randomstring');

const appUser = require('../auth/user');

const _ = require('lodash');

const notificationCommands = require('../config/commands/notificationCommands');

const docTypes = {
    invoice: {
        name: 'invoice',
        prefix: 'INV'
    },
    payment: {
        name: 'payment',
        prefix: 'PYM'
    },
    overpayment: {
        name: 'overpayment',
        prefix: 'OVP'
    },
    underpayment: {
        name: 'underpayment',
        prefix: 'UNP'
    }
}

const lineType = {
    utility: {
        name: undefined,
        enumValue: 102
    },
    rent: {
        name: undefined,
        enumValue: 101,
    }
}

const constructRefNo = (docType) => {
    return docTypes[docType].prefix + '-' + randomstring.generate({
        length: 7,
        charset: 'alphanumeric',
        capitalization: 'uppercase'
    });
};

module.exports = {
    createInvoice: ({ data }) => {

        return new Promise((resolve, reject) => {

            const dateParts = data.date.split(',');
            const docDate = moment().date(new Date().getDay).month(dateParts[0]).year(dateParts[1]).format('YYYY-MM-DD');

            connection.getConnection((err, connection) => {

                connection.beginTransaction((err) => {

                    // Create a document entry
                    connection.query("SELECT * FROM vtenants WHERE pid=? AND current_tenant=?", [
                        appUser.user.pid, true
                    ], (err, tenants, fields) => {
                        if (err)
                            return reject({
                                code: 404,
                                success: false,
                                message: 'Generate invoices failed',
                                invoices: err
                            });
                        

                        tenants.forEach((tenant, i) => {
                            let documentId = uuid();

                            // console.log(moment(new Date(docDate)).tz('UTC').format('MY'), moment(tenant.created_at).tz('UTC').format('MY'));
                            
                            // if (moment(new Date(docDate)).tz('UTC').format() >= moment(tenant.created_at).tz('UTC').format('')) {
                            
                                //Tenant is in the range of invoices being generated for him / her

                                connection.query('CALL `insert_document` (?,?,?,?,?,?)', [
                                    documentId,
                                    constructRefNo(docTypes.invoice.name),
                                    tenant.id,
                                    docDate,
                                    docTypes.invoice.name,
                                    appUser.user.pid
                                ], (err, result, fields) => {
                                    if (err)
                                        return reject({
                                            code: 404,
                                            success: false,
                                            message: 'Generate invoices failed',
                                            invoices: err
                                        });
                                    if (result.affectedRows === 1) {
                                        /** Create new entries**/

                                        // Post rent insert into rent_values
                                        connection.query(" CALL `insert_rent_value` (?,?,?,?,?,?,?,?)", [
                                            uuid(),
                                            tenant.unit_id,
                                            tenant.unit_no,
                                            tenant.id,
                                            (tenant.unit_cost ?
                                                tenant.unit_cost :
                                                0),
                                            docDate,
                                            appUser.user.pid,
                                            tenant.unit_billed
                                        ], (err, results, fields) => {
                                            if (err)
                                                return reject({
                                                    code: 404,
                                                    success: false,
                                                    message: 'Generate invoices failed',
                                                    invoices: err
                                                });

                                            /** This statement should run as a cron at the end of the month ***/
                                            connection.query("SELECT * FROM rent_values WHERE tenant_id=? AND posted=? AND is_billed=?", [
                                                tenant.id, 0, 1
                                            ], (err, results, fields) => {
                                                if (err)
                                                    return reject({
                                                        code: 404,
                                                        success: false,
                                                        message: 'Generate invoices failed',
                                                        invoices: err
                                                    });
                                                
                                                if (results.length) {
                                                    results.forEach((rentValue) => {
                                                        connection.query("CALL `insert_line_item`(?,?,?,?,?,?,?,?,?,?,?)", [
                                                            uuid(),
                                                            appUser.user.pid,
                                                            rentValue.tenant_id,
                                                            documentId,
                                                            rentValue.id,
                                                            'Rent ' + rentValue.unit_no,
                                                            lineType.rent.enumValue,
                                                            `${1}`,
                                                            rentValue.amount,
                                                            rentValue.amount,
                                                            docDate
                                                        ], (err, result, fields) => {
                                                            if (err)
                                                                return reject({
                                                                    code: 404,
                                                                    success: false,
                                                                    message: 'Generate invoices failed',
                                                                    invoices: err
                                                                });
                                                            connection.query("UPDATE rent_values SET posted=? WHERE id=?", [
                                                                1, rentValue.id
                                                            ], (err, result, fields) => {
                                                                if (err)
                                                                    return reject({
                                                                        code: 404,
                                                                        success: false,
                                                                        message: 'Generate invoices failed',
                                                                        invoices: err
                                                                    });
                                                            });
                                                        });
                                                    });
                                                }
                                            });
                                        });

                                        // Post utility readings
                                        connection.query("SELECT * FROM vtenatUtilityReadings WHERE tenant_id=? AND posted=?;", [
                                            tenant.id, 0
                                        ], (err, result, fields) => {
                                            if (err)
                                                return reject({
                                                    code: 404,
                                                    success: false,
                                                    message: 'Generate invoices failed',
                                                    invoices: err
                                                });
                                            result.forEach((utilityReading) => {
                                                connection.query("CALL `insert_line_item` (?,?,?,?,?,?,?,?,?,?,?)", [
                                                    uuid(),
                                                    appUser.user.pid,
                                                    utilityReading.tenant_id,
                                                    documentId,
                                                    utilityReading.id,
                                                    utilityReading.utilityName,
                                                    lineType.utility.enumValue,
                                                    utilityReading.consumption,
                                                    (utilityReading.unit_price ?
                                                        utilityReading.unit_price :
                                                        0),
                                                    (utilityReading.amountDue ?
                                                        utilityReading.amountDue :
                                                        0),
                                                    moment(utilityReading.reading_date, 'DD-MM-YYYY').format('YYYY-MM-DD')
                                                ], (err, result, fields) => {
                                                    if (err)
                                                        return reject({
                                                            code: 404,
                                                            success: false,
                                                            message: 'Generate invoices failed',
                                                            invoices: err
                                                        });
                                                    connection.query("update utility_readings set posted=? where id=?", [
                                                        1, utilityReading.id
                                                    ], (err, result, fields) => {
                                                        if (err)
                                                            return reject({
                                                                code: 404,
                                                                success: false,
                                                                message: 'Generate invoices failed',
                                                                invoices: err
                                                            });
                                                    });
                                                });
                                            }, this);
                                        });

                                    } else {
                                        /*** Do this if document for the month exists and want to add line items ***/
                                        connection.query("SELECT `id` FROM `documents` WHERE `tenant_id` = ? AND date_format(doc_date,'%M %Y') = date_format(?,'%M %Y');", [tenant.id, docDate], (err, result, fields) => {
                                            if (err)
                                                return reject({
                                                    code: 404,
                                                    success: false,
                                                    message: 'Generate invoices failed',
                                                    invoices: err
                                                });
                                            result.forEach((Document) => {
                                                // Post rent insert into rent_values
                                                connection.query(" CALL `insert_rent_value` (?,?,?,?,?,?,?,?)", [
                                                    uuid(),
                                                    tenant.unit_id,
                                                    tenant.unit_no,
                                                    tenant.id,
                                                    (tenant.unit_cost ?
                                                        tenant.unit_cost :
                                                        0),
                                                    docDate,
                                                    appUser.user.pid,
                                                    tenant.unit_billed
                                                ], (err, results, fields) => {
                                                    if (err)
                                                        return reject({
                                                            code: 404,
                                                            success: false,
                                                            message: 'Generate invoices failed',
                                                            invoices: err
                                                        });

                                                    /** This statement should run as a cron at the end of the month ***/
                                                    connection.query("SELECT * FROM rent_values WHERE tenant_id=? AND posted=? AND is_billed=?", [
                                                        tenant.id, 0, 1
                                                    ], (err, results, fields) => {
                                                        if (err)
                                                            return reject({
                                                                code: 404,
                                                                success: false,
                                                                message: 'Generate invoices failed',
                                                                invoices: err
                                                            });
                                                        results.forEach((rentValue) => {
                                                            connection.query("CALL `insert_line_item`(?,?,?,?,?,?,?,?,?,?,?)", [
                                                                uuid(),
                                                                appUser.user.pid,
                                                                rentValue.tenant_id,
                                                                Document.id,
                                                                rentValue.id,
                                                                'Rent ' + rentValue.unit_no,
                                                                lineType.rent.enumValue,
                                                                `${1}`,
                                                                rentValue.amount,
                                                                rentValue.amount,
                                                                docDate
                                                            ], (err, result, fields) => {
                                                                if (err)
                                                                    return reject({
                                                                        code: 404,
                                                                        success: false,
                                                                        message: 'Generate invoices failed',
                                                                        invoices: err
                                                                    });
                                                                connection.query("UPDATE rent_values SET posted=? WHERE id=?", [
                                                                    1, rentValue.id
                                                                ], (err, result, fields) => {
                                                                    if (err)
                                                                        return reject({
                                                                            code: 404,
                                                                            success: false,
                                                                            message: 'Generate invoices failed',
                                                                            invoices: err
                                                                        });
                                                                });
                                                            });
                                                        });
                                                    });
                                                });

                                                // Post utility readings
                                                connection.query("SELECT * FROM `vtenatUtilityReadings` WHERE `tenant_id`=? AND `posted`=? AND `initial_reading`=?;", [
                                                    tenant.id, 0, 0
                                                ], (err, result, fields) => {
                                                    if (err)
                                                        return reject({
                                                            code: 404,
                                                            success: false,
                                                            message: 'Generate invoices failed',
                                                            invoices: err
                                                        });

                                                    if (result.length) {
                                                        result.forEach((utilityReading) => {
                                                            connection.query("CALL `insert_line_item` (?,?,?,?,?,?,?,?,?,?,?)", [
                                                                uuid(),
                                                                appUser.user.pid,
                                                                utilityReading.tenant_id,
                                                                Document.id,
                                                                utilityReading.id,
                                                                utilityReading.utilityName,
                                                                lineType.utility.enumValue,
                                                                utilityReading.consumption,
                                                                (utilityReading.unit_price ?
                                                                    utilityReading.unit_price :
                                                                    0),
                                                                (utilityReading.amountDue ?
                                                                    utilityReading.amountDue :
                                                                    0),
                                                                moment(utilityReading.reading_date, 'DD-MM-YYYY').format('YYYY-MM-DD')

                                                            ], (err, result, fields) => {
                                                                if (err)
                                                                    return reject({
                                                                        code: 404,
                                                                        success: false,
                                                                        message: 'Generate invoices failed',
                                                                        invoices: err
                                                                    });
                                                                connection.query("update utility_readings set posted=? where id=?", [
                                                                    1, utilityReading.id
                                                                ], (err, result, fields) => {
                                                                    if (err)
                                                                        return reject({
                                                                            code: 404,
                                                                            success: false,
                                                                            message: 'Generate invoices failed',
                                                                            invoices: err
                                                                        });
                                                                });
                                                            });
                                                        }, this);
                                                    }

                                                });

                                            }, this);
                                        });
                                    }
                                });
                            // }
                        }, this);
                    });

                    // Commit
                    connection.commit((err) => {
                        if (err) {
                            return connection.rollback(() => {
                                return reject({
                                    code: 404,
                                    success: false,
                                    message: 'Generate invoices failed',
                                    invoices: {
                                        data: err
                                    }
                                });
                            });
                        } else {
                            return resolve({
                                code: 200,
                                success: true,
                                iterable: false,
                                message: 'Genenrate invoices suceess',
                                invoices: {
                                    data: [data]
                                }
                            });
                        }
                    });
                });
                connection.release();
            });

        });
    },
    payInvoice: ({ data }) => {
        return new Promise((resolve, reject) => {
            
            const journalId = uuid();

            const receivedAmount = data.amount;

            data.paymentRef = constructRefNo('payment');

            let documentId = uuid();

            data.receivingDate = moment(data.receivingDate, 'DD/MM/YYYY').format('YYYY-MM-DD');

            let transactingAmount = parseFloat(receivedAmount);
            let __result = [];

            let openInvoices;
            let balance_at_hand = transactingAmount;
            let remainingBalance = 0;
            let totalOpenInvoices = 0;
            let paymentComplete = false;

            connection.getConnection((err, connection) => {

                connection.beginTransaction((err) => {
                    if (err)
                        throw err;

                    var _effectPayment = () => {

                        // Get balance brought forward
                        var getBalanceBroughtForward = ((data) => {

                            if (data.tenant_id) {

                                connection.query("SELECT `balance` FROM `tenant_balances` WHERE `tenant_id`=?;", [data.tenant_id], (err, results, fields) => {
                                    if (err)
                                        throw err;

                                    if (results.length) {

                                        let bbfResults = _.first(results);
                                        transactingAmount = parseFloat(receivedAmount) + parseFloat(bbfResults.balance);

                                        connection.query("UPDATE `tenant_balances` SET  `balance` = ? WHERE `tenant_id` = ? ;", [
                                            0, data.tenant_id
                                        ], (err, results, fields) => {
                                            if (err)
                                                throw err;

                                            // Create a document entry
                                            createDocumentEntry();
                                        });

                                    } else {
                                        // Create a document entry
                                        createDocumentEntry();
                                    }
                                });
                            }

                        })(data)

                        // Create a document entry for the payment
                        var createDocumentEntry = () => {

                            connection.query("INSERT INTO `documents` (`id`,`pid`,`doc_ref`,`tenant_id`,parent_id,`amount`,`" +
                                "discriminator`,`doc_date`) values(?,?,?,?,?,?,?,?);", [
                                    documentId,
                                    data.pid?data.pid:appUser.user.pid,
                                    data.paymentRef,
                                    data.tenant_id,
                                    data.document_id,
                                    data.fromAcBal?transactingAmount:receivedAmount,
                                    docTypes.payment.name,
                                    data.receivingDate
                                ], (err, results, fields) => {
                                    if (err)
                                        throw err;

                                    // Credit the amount
                                    //Do not credit when paying from a/c balance, the overpayment had already been credited
                                    creditAccount();

                                });

                        }

                        // Credit the amount
                        var creditAccount = () => {

                            connection.query("INSERT INTO `journal` (`pid`,`id`,`tid`,`document_id`,`transaction_type`,`account_type`,`amoun" +
                                "t`,`transaction_date`,`transaction_ref`,`receiving_acc`) VALUES(?,?,?,?,?,?,?,?,?," +
                                "?);", [
                                    data.pid?data.pid:appUser.user.pid,
                                    documentId,
                                    data.tenant_id,
                                    data.document_id,
                                    'credit',
                                    'receivable',
                                    data.fromAcBal?transactingAmount:receivedAmount,
                                    data.receivingDate,
                                    data.transactionRef,
                                    data.receivingAcc
                                ], (err, results, fields) => {
                                    if (err)
                                        throw err;

                                    //Debit the amount
                                    debitAccount();

                                });

                        }

                        // Debit the amount
                        var debitAccount = () => {

                            connection.query("INSERT INTO `journal` (pid,id,document_id,tid,transaction_type,account_type,amount,transacti" +
                                "on_date,transaction_ref) values(?,?,?,?,?,?,?,?,?);", [
                                    data.pid?data.pid:appUser.user.pid,
                                    journalId,
                                    data.document_id,
                                    data.tenant_id,
                                    'debit',
                                    'payable',
                                    data.fromAcBal?transactingAmount:receivedAmount,
                                    data.receivingDate,
                                    data.transactionRef
                                ], (err, results, fields) => {
                                    if (err)
                                        throw err;

                                    // Get all items with pending balances
                                    getPendingBalances();

                                });

                        }

                        // Get all open invoices
                        /*** If you want to pay through a single document, add the document_id to the where clause  */
                        var getPendingBalances = () => {

                            connection.query("SELECT * FROM `line_items` WHERE `tenant_id`=? AND `paid_to_date` < `amount` ORDER BY posting_date DESC;", [data.tenant_id], (err, results, fields) => {
                                if (err)
                                    throw err;

                                if (results.length > 0) {
                                    openInvoices = results;

                                    /** Process payment for each line item **/

                                    // Reduce balance on each line              
                                    reduceBalanceOnItem()

                                } else {
                                    remainingBalance = data.fromAcBal?transactingAmount:receivedAmount;

                                    // Persist any remaining balance
                                    effectRemainingBalance();

                                }
                            });

                        }

                        // Reduce balance on line item
                        var reduceBalanceOnItem = () => {

                            totalOpenInvoices = (openInvoices.length - 1);
                            
                            let reduceBalanceOnLineItem = (balance_at_hand, n) => {
                                if (n < 0) {

                                    // Effect payment on line item
                                    effectPaymentOnLineItem();

                                    return balance_at_hand;
                                } else {
                                    if (balance_at_hand > 0) {

                                        if (openInvoices[n].paid_to_date > 0) {

                                            balance_at_hand += openInvoices[n].paid_to_date;
                                            openInvoices[n].paid_to_date = 0;

                                            if (balance_at_hand >= openInvoices[n].amount) {
                                                openInvoices[n].paid_to_date = openInvoices[n].amount;
                                            } else {
                                                if (balance_at_hand > 0) {
                                                    openInvoices[n].paid_to_date = balance_at_hand;
                                                }
                                            }

                                            // Reccur
                                            return reduceBalanceOnLineItem((balance_at_hand - openInvoices[n].amount), (n - 1));

                                        } else {
                                            if (balance_at_hand > 0 && balance_at_hand > openInvoices[n].amount) {
                                                openInvoices[n].paid_to_date = openInvoices[n].amount;
                                            } else {
                                                if (balance_at_hand > 0) {
                                                    openInvoices[n].paid_to_date = balance_at_hand;
                                                }
                                            }

                                            //	Reccur
                                            return reduceBalanceOnLineItem((balance_at_hand - openInvoices[n].amount), (n - 1));
                                        }
                                    } else {

                                        // Effect payment on line item
                                        effectPaymentOnLineItem();

                                        return balance_at_hand;
                                    }
                                }
                            }

                            remainingBalance = reduceBalanceOnLineItem(transactingAmount, totalOpenInvoices);

                        }


                        // Effect payment on line item

                        var effectPaymentOnLineItem = () => {
                            openInvoices.forEach((element, i) => {
                                connection.query("update line_items set paid_to_date = ?,initiated_payment = ? where id = ?;", [
                                    element.paid_to_date, 1, element.id
                                ], (err, results, fields) => {
                                    if (err)
                                        throw err;
                                    if ((openInvoices.length - 1) === i) {
                                        // Effect any remaining balance
                                        if (remainingBalance > 0) {
                                            effectRemainingBalance();
                                        } else {
                                            commitTransaction();
                                        }
                                    }
                                });
                            });

                        }

                        // Persist any remaing balance
                        var effectRemainingBalance = () => {

                            let date = moment().format('YYYY-MM-DD');

                            // credit the journal with the positive balance
                            if (remainingBalance > 0) {
                                // Credit the account with the overpaid amount
                                connection.query("insert into journal (id,pid,document_id,tid,transaction_type,account_type,amount,balance_bro" +
                                    "ught_forward,transaction_date,transaction_ref,receiving_acc) values(?,?,?,?,?,?,?,?,?,?,?);", [
                                        uuid(),
                                        data.pid?data.pid:appUser.user.pid,
                                        documentId,
                                        data.tenant_id,
                                        'credit',
                                        'receivable',
                                        remainingBalance,
                                        remainingBalance,
                                        date,
                                        constructRefNo('overpayment'),
                                        'Overpayment'
                                    ], (err, results, fields) => {
                                        if (err)
                                            throw err;

                                        //set the overpayment value
                                        connection.query('CALL insert_balance(?,?,?,?)', [
                                            uuid(),
                                            data.tenant_id,
                                            appUser.user.pid,
                                            remainingBalance
                                        ], (err, results, fields) => {
                                            if (err)
                                                throw err;

                                            // Commit the transaction
                                            commitTransaction();

                                        });
                                    });
                            }

                        }

                        // Commit transaction

                        var commitTransaction = () => {

                            connection.commit((err) => {
                                if (err) {
                                    throw err;

                                    return connection.rollback(() => {
                                        return reject({
                                            code: 404,
                                            success: false,
                                            message: 'Record payment failed',
                                            payments: {
                                                data: err
                                            }
                                        });
                                    });

                                } else {

                                    data.tenantBalance = (remainingBalance > 0 ? (remainingBalance * -1) : (_(openInvoices).sumBy('amount') - _(openInvoices).sumBy('paid_to_date')));
                                    
                                    // Notify payment has been recorded - Always send a receipt via email
                                    data.amount = data.fromAcBal ? transactingAmount : receivedAmount;
                                    
                                    const tenant = require('../models/tenant.model');

                                    tenant.get({ id: data.tenant_id }).then((tenant) => {

                                        if (!_.isEmpty(tenant.tenants.data)) {
                                            tenant = _.first(tenant.tenants.data);

                                            // Email the receipt
                                            const amqp = require('amqplib/callback_api');
                                            amqp.connect('amqp://localhost', (err, conn) => {
                                                conn.createChannel((err, ch) => {
                                                    if (err)
                                                        throw err;

                                                    const msgs = {
                                                        EMAIL_PAYMENT_RECEIPT: 'EMAIL_PAYMENT_RECEIPT'
                                                    }
                                                    const q = 'defaultQueue';
                                                    const msg = JSON.stringify({msg:msgs.EMAIL_PAYMENT_RECEIPT,appUser:appUser.user,data:data, tenant: tenant});

                                                    ch.assertQueue(q, { durable: false });
                                                    ch.sendToQueue(q, Buffer.from(msg));
                                                    setTimeout(() => { conn.close(); return; }, 500);
                                                });
                                            });
                                        
                                            const notification = require('../models/notify.model');
                                            notification.checkSMSSetting(notificationCommands.PAYMENT_SAVED).then(toSMS => {
                                                if (toSMS) {
                                                    const mobileNo = tenant.contactPhone.replace('0', '+254');

                                                    const numeral = require('numeral');
                                                    
                                                    const message = `${tenant.buildingName}, Your payment of ${numeral(data.fromAcBal ? transactingAmount : receivedAmount).format('0,0.00')}, has been recorded Ref ${data.paymentRef}. We have updated your account and your balance is KES ${numeral(data.tenantBalance).format('0,0.00')}`

                                                    notification.sendSMS(tenant.id, mobileNo, message);
                                                }
                                            });
                                        }
                        
                                    }).catch((err) => {
                                        return reject(err);
                                    });

                                    return resolve({
                                        code: 200,
                                        success: true,
                                        message: 'Recorded payment successfully',
                                        iterable: false,
                                        payments: {
                                            data: [data]
                                        }
                                    });
                                }
                            });

                        }

                    }

                    _effectPayment();

                });

                connection.release();

            });

        });
    },
    cleanupInvoices: () => {
        return new Promise((resolve, reject) => {
            DB.raw("DELETE FROM documents WHERE id NOT IN (SELECT id FROM vdocuments) AND discriminator NOT LIKE 'payment' AND pid=?;", [appUser.user.pid]).then((result) => {
                return resolve(true);
            }).catch((err) => {
                return reject(err);
            });
        });        
      }
}