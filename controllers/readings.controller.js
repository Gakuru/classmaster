const readings = require('../models/readings.model');

const command = require('../config/commands/commands');

module.exports = {
  get: ({ page, id, callback }) => {
    readings.get({ page, id, command: id ? command.GET_BY_ID : command.GET }).then((result) => {
      callback(result);
    }).catch((err) => {
      callback(err);
    });
  },
  find: ({ param, page, callback }) => {
    readings.find({ param, page }).then((result) => {
      callback(result);
    }).catch((err) => {
      callback(err);
    });
  },
  create: ({ data, callback }) => {
    readings.save({ data }).then((result) => {
      callback(result);
    }).catch((err) => {
      callback(err);
    });
  },
  update: ({ data, callback }) => {
    readings.save({ data, command: command.UPDATE }).then((result) => {
      callback(result);
    }).catch((err) => {
      callback(err);
    });
  },
  remove: ({ data, callback }) => {
    readings.remove({ data }).then((result) => {
      callback(result)
    }).catch((err) => {
      callback(err);
    });
  },
};
