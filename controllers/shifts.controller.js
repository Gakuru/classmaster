const shifts = require('../models/shifts.model');

const command = require('../config/commands/commands');

module.exports = {
  get: ({ page, id, callback }) => {
    shifts.get({ page, id, command: id ? command.GET_BY_ID : command.GET }).then((result) => {
      callback(result);
    }).catch((err) => {
      callback(err);
    });
  },
  find: ({ param, page, callback }) => {
    shifts.find({ param, page }).then((result) => {
      callback(result);
    }).catch((err) => {
      callback(err);
    });
  },
  create: ({ data, callback }) => {
    shifts.save({ data }).then((result) => {
      callback(result);
    }).catch((err) => {
      callback(err);
    });
  },
  update: ({ data, callback }) => {
    shifts.save({ data, command: command.UPDATE }).then((result) => {
      callback(result);
    }).catch((err) => {
      callback(err);
    });
  },
  remove: ({ data, callback }) => {
    shifts.remove({ data }).then((result) => {
      callback(result)
    }).catch((err) => {
      callback(err);
    });
  },
};
