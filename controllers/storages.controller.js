const storages = require('../models/storages.model');

const command = require('../config/commands/commands');

module.exports = {
  get: ({ page, id, callback }) => {
    storages.get({ page, id, command: id ? command.GET_BY_ID : command.GET }).then((result) => {
      callback(result);
    }).catch((err) => {
      callback(err);
    });
  },
  find: ({ param, page, callback }) => {
    storages.find({ param, page }).then((result) => {
      callback(result);
    }).catch((err) => {
      callback(err);
    });
  },
  create: ({ data, callback }) => {
    storages.save({ data }).then((result) => {
      callback(result);
    }).catch((err) => {
      callback(err);
    });
  },
  update: ({ data, callback }) => {
    storages.save({ data, command: command.UPDATE }).then((result) => {
      callback(result);
    }).catch((err) => {
      callback(err);
    });
  },
  remove: ({ data, callback }) => {
    storages.remove({ data }).then((result) => {
      callback(result)
    }).catch((err) => {
      callback(err);
    });
  },
};
