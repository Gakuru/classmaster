const account = require('../models/account.model');
const command = require('../config/commands/commands');

module.exports = {
  getNonificationSettings: ({ category, callback }) => {
    account.getNonificationSettings({ category }).then((result) => {
      callback(result);
    }).catch((err) => {
      callback(err);
    });
  },
  getAccountBalabce: ({ callback }) => {
    account.getAccountBalabce().then((result) => {
      callback(result);
    }).catch((err) => {
      callback(err);
    });
  },
  // getByStatus: ({ data, callback }) => {
  //   unit.getByStatus({ data }).then((result) => {
  //     callback(result);
  //   }).catch((err) => {
  //     callback(err);
  //   });
  // },
  // export: (callback) => {
    
  // },
  // find: ({ param, page, callback }) => {
  //   unit.find({ param, page }).then((result) => {
  //     callback(result);
  //   }).catch((err) => {
  //     callback(err);
  //   });
  // },
  saveNotificationSettings: ({ data, callback }) => {
    account.saveNotificationSettings({ data }).then((result) => {
      callback(result);
    }).catch((err) => {
      callback(err);
    });
  }
  // ,
  // update: ({data, callback}) => {
  //   unit.save({ data, command:command.UPDATE }).then((result) => {
  //     callback(result);
  //   }).catch((err) => {
  //     callback(err);
  //   });
  // },
  // remove: ({data, callback}) => {
  //   unit.remove({ data }).then((result) => {
  //     callback(result)
  //   }).catch((err) => {
  //     callback(err);
  //   });
  // }
};
