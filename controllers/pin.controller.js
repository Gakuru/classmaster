const pin = require('../models/pin.model');

const command = require('../config/commands/commands');

module.exports = {
  get: ({ data, callback }) => {
    pin.get(data).then((result) => {
      callback(result);
    }).catch((err) => {
      callback(err);
    });
  },
}
