const analysis = require('../models/analysis.model');

module.exports = {
    getBuildingTemplate: ({id, callback}) => {
        analysis.analyseBuilding({id}).then((result) => {
                callback(result);
        }).catch((err) => {
            callback(err);
        });
    },
    analyseShifts: ({shiftId, callback}) => {
        analysis.analyseShifts({shiftId}).then((result) => {
                callback(result);
        }).catch((err) => {
                callback(err);
        });
    },
    analyseUnit: ({id, callback}) => {
        analysis.analyseUnit({id}).then((result) => {
                callback(result);
        }).catch((err) => {
                callback(err);
        });
    },
    analysePayment: ({id, callback}) => {
        analysis.analysePayment({id}).then((result) => {
                callback(result);
        }).catch((err) => {
                callback(err);
        });
    },
    collectionsByPaymentMode: ({year, callback}) => {
        analysis.collectionsByPaymentMode({ year }).then((result) => {
                callback(result);
        }).catch((err) => {
                callback(err);
        });
    },
    collectionsByUnitType: ({year, callback}) => {
        analysis.collectionsByUnitType({ year }).then((result) => {
                callback(result);
        }).catch((err) => {
                callback(err);
        });
    },
    collectionsByApartmentPerYear: ({year,callback}) => {
        analysis.collectionsByApartmentPerYear({year}).then((result) => {
                callback(result);
        }).catch((err) => {
                callback(err);
        });
    }
};
