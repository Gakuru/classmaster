const pumps = require('../models/pumps.model');

const command = require('../config/commands/commands');

module.exports = {
  get: ({ page, id, callback }) => {
    pumps.get({ page, id, command: id ? command.GET_BY_ID : command.GET }).then((result) => {
      callback(result);
    }).catch((err) => {
      callback(err);
    });
  },
  getNozzle: ({ id, callback }) => {
    pumps.getNozzle({ id }).then((result) => {
      callback(result);
    }).catch((err) => {
      callback(err);
    });
  },
  find: ({ param, page, callback }) => {
    pumps.find({ param, page }).then((result) => {
      callback(result);
    }).catch((err) => {
      callback(err);
    });
  },
  create: ({ data, callback }) => {
    pumps.save({ data }).then((result) => {
      callback(result);
    }).catch((err) => {
      callback(err);
    });
  },
  update: ({ data, callback }) => {
    pumps.save({ data, command: command.UPDATE }).then((result) => {
      callback(result);
    }).catch((err) => {
      callback(err);
    });
  },
  remove: ({ data, callback }) => {
    pumps.remove({ data }).then((result) => {
      callback(result)
    }).catch((err) => {
      callback(err);
    });
  },
  removeNozzle: ({ data, callback }) => {
    pumps.removeNozzle({ data }).then((result) => {
      callback(result)
    }).catch((err) => {
      callback(err);
    });
  },
};
