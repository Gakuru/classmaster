const reports = require('../models/reports.model');

module.exports = {
  getReports: ({ callback }) => {
    reports.getReports().then((result) => {
      callback(result);
    }).catch((err) => {
      callback(err);
    });
  },
  getReportUrl: ({ data, callback }) => {
    reports.getReportUrl({ data }).then((result) => {
      callback(result);
    }).catch((err) => {
      callback(err);
    });
  }
};
