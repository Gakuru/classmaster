const rollcall = require('../models/rollcall.model');

const command = require('../config/commands/commands');

module.exports = {
  saveRollcall: ({ data, callback }) => {
    rollcall.saveRollcall(data).then((result) => {
      callback(result);
    }).catch((err) => {
      callback(err);
    });
  },
}
