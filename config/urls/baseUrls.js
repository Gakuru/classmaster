const inDevMode = process.env.IN_DEV_MODE === 'false' ? false : true;
const remote = process.env.REMOTE_API;
const __urls = {
    localhost: `http://localhost:${process.env.API_PORT}`,
    listingsLocalhost: `http://localhost:${process.env.LISTINGS_PORT}`,
    secureRemote: process.env.API_URL,
    listingsSecureRemote: process.env.LISTINGS_API_URL,
    db: {
        localhost: 'localhost',
        remote: process.env.DB_HOST
    }
}

module.exports = {

    baseUrl: (function () {
        if (inDevMode)
            if (remote)
                return __urls.localhost;
        else
            return __urls.secureRemote;
    else
        return __urls.secureRemote;
    })(),

    listingsBaseUrl: (function () {
        if (inDevMode) 
            return __urls.listingsLocalhost;
        else
            return __urls.listingsSecureRemote;
    })(),
    
    reportsUrl: (function () {
        if (inDevMode)
            return __urls.localhost;
        else
            return __urls.secureRemote
    })(),
    
    dbHost: (function () {
        if (inDevMode && !remote) 
            return __urls.db.localhost;
        else 
            return __urls.db.remote;
    })(),

    __urls: __urls

};
