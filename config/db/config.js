const url = require('../urls/baseUrls');

module.exports = {
    baseUrl: url.baseUrl,
    connectionString: {
        host: url.dbHost,
        user: process.env.DB_USER,
        password: process.env.DB_PASS,
        database: process.env.DB_NAME
    }
};
