const mysql = require('mysql');
const conn = require('./config');

const connection = mysql.createPool({
  connectionLimit: 30,
  multipleStatements:true,
  host: conn.connectionString.host,
  user: conn.connectionString.user,
  password: conn.connectionString.password,
  database: conn.connectionString.database
});

module.exports = connection;
