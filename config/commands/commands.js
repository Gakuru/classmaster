module.exports = {
    SAVE: 'SAVE',
    UPDATE: 'UPDATE',
    GET: 'GET',
    GET_BY_ID: 'GET_BY_ID',
    GET_BY_STATUS:'GET_BY_STATUS',
    FIND: 'FIND',
    REMOVE: 'REMOVE',
    PAY:'PAY'
}