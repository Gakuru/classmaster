module.exports = {
    TENANT_SAVED: 'tenantSaved',
    PAYMENT_SAVED: 'paymentSaved',
    BALANCE_REMINDER: 'balanceReminder'
}