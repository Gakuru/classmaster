const connection = require('../config/db/mysql');

const DB = require('../models/db');

const moment = require('moment-timezone');

const uuid = require('uuid/v1');

const randomstring = require('randomstring');

const _ = require('lodash');

const notificationCommands = require('../config/commands/notificationCommands');

const docTypes = {
    invoice: {
        name: 'invoice',
        prefix: 'INV'
    },
    payment: {
        name: 'payment',
        prefix: 'PYM'
    },
    overpayment: {
        name: 'overpayment',
        prefix: 'OVP'
    },
    underpayment: {
        name: 'underpayment',
        prefix: 'UNP'
    }
};

const lineType = {
    utility: {
        name: undefined,
        enumValue: 102
    },
    rent: {
        name: undefined,
        enumValue: 101,
    }
}

const queryLimit = 100;

const constructRefNo = (docType) => {
    return docTypes[docType].prefix + '-' + randomstring.generate({
        length: 7,
        charset: 'alphanumeric',
        capitalization: 'uppercase'
    });
}

const createInvoices = (offset=0,limit=queryLimit,iterable=true) => {

    return new Promise((resolve, reject) => {

        const docDate = moment().format('YYYY-MM-DD');

        connection.getConnection((err, connection) => {

            connection.beginTransaction((err) => {

                // Create a document entry
                connection.query("SELECT usr.name as userName, vten.* FROM vtenants vten LEFT JOIN users usr on usr.pid = vten.pid WHERE vten.current_tenant=? and vten.deleted=? and vten.pid = 2 limit ?,?", [
                    true,false,offset,limit
                ], (err, tenants, fields) => {
                    if (err)
                        return reject({
                            code: 404,
                            success: false,
                            message: 'Generate invoices failed',
                            invoices: err
                        });
                    
                    iterable = tenants.length ? true : false;

                    let account = {
                        pid: '',
                        name: ''
                    }
                    
                    // console.log(tenants.length,offset,limit,iterable);

                    if (iterable) {

                        account.pid = _.first(tenants).pid;
                        account.name = _.first(tenants).userName;

                        tenants.forEach((tenant, i) => {
                        let documentId = uuid();

                        // console.log(moment(new Date(docDate)).tz('UTC').format('MY'), moment(tenant.created_at).tz('UTC').format('MY'));
                        
                        if (moment(new Date(docDate)).format('MY') >= moment(tenant.created_at).format('MY')) {
                            //Tenant is in the range of invoices being generated for him / her

                            connection.query('CALL `insert_document` (?,?,?,?,?,?)', [
                                documentId,
                                constructRefNo(docTypes.invoice.name),
                                tenant.id,
                                docDate,
                                docTypes.invoice.name,
                                tenant.pid
                            ], (err, result, fields) => {
                                if (err)
                                    return reject({
                                        code: 404,
                                        success: false,
                                        message: 'Generate invoices failed',
                                        invoices: err
                                    });
                                if (result.affectedRows === 1) {
                                    /** Create new entries**/

                                    // Post rent insert into rent_values
                                    connection.query(" CALL `insert_rent_value` (?,?,?,?,?,?,?,?)", [
                                        uuid(),
                                        tenant.unit_id,
                                        tenant.unit_no,
                                        tenant.id,
                                        (tenant.unit_cost ?
                                            tenant.unit_cost :
                                            0),
                                        docDate,
                                        tenant.pid,
                                        tenant.unit_billed
                                    ], (err, results, fields) => {
                                        if (err)
                                            return reject({
                                                code: 404,
                                                success: false,
                                                message: 'Generate invoices failed',
                                                invoices: err
                                            });

                                        /** This statement should run as a cron at the end of the month ***/
                                        connection.query("SELECT * FROM rent_values WHERE tenant_id=? AND posted=? AND is_billed=?", [
                                            tenant.id, 0, 1
                                        ], (err, results, fields) => {
                                            if (err)
                                                return reject({
                                                    code: 404,
                                                    success: false,
                                                    message: 'Generate invoices failed',
                                                    invoices: err
                                                });
                                            
                                            if (results.length) {
                                                results.forEach((rentValue) => {
                                                    connection.query("CALL `insert_line_item`(?,?,?,?,?,?,?,?,?,?,?)", [
                                                        uuid(),
                                                        tenant.pid,
                                                        rentValue.tenant_id,
                                                        documentId,
                                                        rentValue.id,
                                                        'Rent ' + rentValue.unit_no,
                                                        lineType.rent.enumValue,
                                                        `${1}`,
                                                        rentValue.amount,
                                                        rentValue.amount,
                                                        docDate
                                                    ], (err, result, fields) => {
                                                        if (err)
                                                            return reject({
                                                                code: 404,
                                                                success: false,
                                                                message: 'Generate invoices failed',
                                                                invoices: err
                                                            });
                                                        connection.query("UPDATE rent_values SET posted=? WHERE id=?", [
                                                            1, rentValue.id
                                                        ], (err, result, fields) => {
                                                            if (err)
                                                                return reject({
                                                                    code: 404,
                                                                    success: false,
                                                                    message: 'Generate invoices failed',
                                                                    invoices: err
                                                                });
                                                        });
                                                    });
                                                });
                                            }
                                        });
                                    });

                                    // Post utility readings
                                    connection.query("SELECT * FROM vtenatUtilityReadings WHERE tenant_id=? AND posted=?;", [
                                        tenant.id, 0
                                    ], (err, result, fields) => {
                                        if (err)
                                            return reject({
                                                code: 404,
                                                success: false,
                                                message: 'Generate invoices failed',
                                                invoices: err
                                            });
                                        result.forEach((utilityReading) => {
                                            connection.query("CALL `insert_line_item` (?,?,?,?,?,?,?,?,?,?,?)", [
                                                uuid(),
                                                tenant.pid,
                                                utilityReading.tenant_id,
                                                documentId,
                                                utilityReading.id,
                                                utilityReading.utilityName,
                                                lineType.utility.enumValue,
                                                utilityReading.consumption,
                                                (utilityReading.unit_price ?
                                                    utilityReading.unit_price :
                                                    0),
                                                (utilityReading.amountDue ?
                                                    utilityReading.amountDue :
                                                    0),
                                                moment(utilityReading.reading_date, 'DD-MM-YYYY').format('YYYY-MM-DD')
                                            ], (err, result, fields) => {
                                                if (err)
                                                    return reject({
                                                        code: 404,
                                                        success: false,
                                                        message: 'Generate invoices failed',
                                                        invoices: err
                                                    });
                                                connection.query("update utility_readings set posted=? where id=?", [
                                                    1, utilityReading.id
                                                ], (err, result, fields) => {
                                                    if (err)
                                                        return reject({
                                                            code: 404,
                                                            success: false,
                                                            message: 'Generate invoices failed',
                                                            invoices: err
                                                        });
                                                });
                                            });
                                        }, this);
                                    });

                                } else {
                                    /*** Do this if document for the month exists and want to add line items ***/
                                    connection.query("SELECT `id` FROM `documents` WHERE `tenant_id` = ? AND date_format(doc_date,'%M %Y') = date_format(?,'%M %Y');", [tenant.id, docDate], (err, result, fields) => {
                                        if (err)
                                            return reject({
                                                code: 404,
                                                success: false,
                                                message: 'Generate invoices failed',
                                                invoices: err
                                            });
                                        result.forEach((Document) => {
                                            // Post rent insert into rent_values
                                            connection.query(" CALL `insert_rent_value` (?,?,?,?,?,?,?,?)", [
                                                uuid(),
                                                tenant.unit_id,
                                                tenant.unit_no,
                                                tenant.id,
                                                (tenant.unit_cost ?
                                                    tenant.unit_cost :
                                                    0),
                                                docDate,
                                                tenant.pid,
                                                tenant.unit_billed
                                            ], (err, results, fields) => {
                                                if (err)
                                                    return reject({
                                                        code: 404,
                                                        success: false,
                                                        message: 'Generate invoices failed',
                                                        invoices: err
                                                    });

                                                /** This statement should run as a cron at the end of the month ***/
                                                connection.query("SELECT * FROM rent_values WHERE tenant_id=? AND posted=? AND is_billed=?", [
                                                    tenant.id, 0, 1
                                                ], (err, results, fields) => {
                                                    if (err)
                                                        return reject({
                                                            code: 404,
                                                            success: false,
                                                            message: 'Generate invoices failed',
                                                            invoices: err
                                                        });
                                                    results.forEach((rentValue) => {
                                                        connection.query("CALL `insert_line_item`(?,?,?,?,?,?,?,?,?,?,?)", [
                                                            uuid(),
                                                            tenant.pid,
                                                            rentValue.tenant_id,
                                                            Document.id,
                                                            rentValue.id,
                                                            'Rent ' + rentValue.unit_no,
                                                            lineType.rent.enumValue,
                                                            `${1}`,
                                                            rentValue.amount,
                                                            rentValue.amount,
                                                            docDate
                                                        ], (err, result, fields) => {
                                                            if (err)
                                                                return reject({
                                                                    code: 404,
                                                                    success: false,
                                                                    message: 'Generate invoices failed',
                                                                    invoices: err
                                                                });
                                                            connection.query("UPDATE rent_values SET posted=? WHERE id=?", [
                                                                1, rentValue.id
                                                            ], (err, result, fields) => {
                                                                if (err)
                                                                    return reject({
                                                                        code: 404,
                                                                        success: false,
                                                                        message: 'Generate invoices failed',
                                                                        invoices: err
                                                                    });
                                                            });
                                                        });
                                                    });
                                                });
                                            });

                                            // Post utility readings
                                            connection.query("SELECT * FROM `vtenatUtilityReadings` WHERE `tenant_id`=? AND `posted`=? AND `initial_reading`=?;", [
                                                tenant.id, 0, 0
                                            ], (err, result, fields) => {
                                                if (err)
                                                    return reject({
                                                        code: 404,
                                                        success: false,
                                                        message: 'Generate invoices failed',
                                                        invoices: err
                                                    });
                                                if (result.length) {
                                                    result.forEach((utilityReading) => {
                                                        connection.query("CALL `insert_line_item` (?,?,?,?,?,?,?,?,?,?,?)", [
                                                            uuid(),
                                                            tenant.pid,
                                                            utilityReading.tenant_id,
                                                            Document.id,
                                                            utilityReading.id,
                                                            utilityReading.utilityName,
                                                            lineType.utility.enumValue,
                                                            utilityReading.consumption,
                                                            (utilityReading.unit_price ?
                                                                utilityReading.unit_price :
                                                                0),
                                                            (utilityReading.amountDue ?
                                                                utilityReading.amountDue :
                                                                0),
                                                            moment(utilityReading.reading_date, 'DD-MM-YYYY').format('YYYY-MM-DD')

                                                        ], (err, result, fields) => {
                                                            if (err)
                                                                return reject({
                                                                    code: 404,
                                                                    success: false,
                                                                    message: 'Generate invoices failed',
                                                                    invoices: err
                                                                });
                                                            connection.query("update utility_readings set posted=? where id=?", [
                                                                1, utilityReading.id
                                                            ], (err, result, fields) => {
                                                                if (err)
                                                                    return reject({
                                                                        code: 404,
                                                                        success: false,
                                                                        message: 'Generate invoices failed',
                                                                        invoices: err
                                                                    });
                                                            });
                                                        });
                                                    }, this);
                                                }

                                            });

                                        }, this);
                                    });
                                }
                            });
                        } 
                    }, this);

                        // Email the newly created invoices
                        const amqp = require('amqplib/callback_api');
                        amqp.connect('amqp://localhost', (err, conn) => {
                            conn.createChannel((err, ch) => {
                                if (err)
                                    throw err;

                                const msgs = {
                                    EMAIL_NEW_INVOICES: 'EMAIL-NEW-INVOICES'
                                }
                                const q = 'defaultQueue';
                                const msg = JSON.stringify({msg:msgs.EMAIL_NEW_INVOICES,appUser:account,docDate:docDate});

                                ch.assertQueue(q, { durable: false });
                                ch.sendToQueue(q, Buffer.from(msg));
                            });
                            setTimeout(() => { conn.close(); return; }, 500);
                        });

                        return resolve(createInvoices((limit + 1), (limit+queryLimit)), iterable);
                        
                    } else {
                        console.log('Done generating invoices at: ',moment().format('DD-MM-YYYY HH:mm:ss'));
                        return resolve(true);
                    }
                    
                });

                // Commit
                connection.commit((err) => {
                    if (err) {
                        return connection.rollback(() => {
                            return reject({
                                code: 404,
                                success: false,
                                message: 'Generate invoices failed',
                                err: err
                            });
                        });
                    } else {
                        return resolve({
                            code: 200,
                            success: true,
                            iterable: false,
                            message: 'Genenrate invoices suceess'
                        });
                    }
                });
            });
            connection.release();
        });

    });
}

createInvoices();