const express = require('express');
const router = express.Router();
const account = require('../controllers/account.controller');

router.get('/', (req, res) => {
  res.send('RenterKE');
});

router.get('/account/settings/notification', (req, res) => {
  account.getNonificationSettings({
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/account/balance', (req, res) => {
  account.getAccountBalabce({
    callback: (result) => {
      res.json(result);
    }
  });
});

router.post('/account/settings/notification', (req, res) => {
  account.saveNotificationSettings({
    data: {
      category: req.body.category,
      settingKey: req.body.settingKey,
      balanceReminder: req.body.balanceReminder,
      paymentSaved: req.body.paymentSaved,
      tenantSaved: req.body.tenantSaved
    },
    callback: (result) => {
      res.json(result);
    }
  });
});

module.exports = router;
