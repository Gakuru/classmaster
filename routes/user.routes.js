const express = require('express');
const router = express.Router();

const common = require('../utils/common');

const UserCtrl = require('../controllers/user.controller');

router.get('/', (req, res) => {
    res.send(common.AppName);
});

router.get('/users/:id', (req, res) => {
    UserCtrl.get({
        id: req.params.id,
        callback: (result) => {
            res.json(result);
        }
    });
});

router.get('/users', (req, res) => {
    UserCtrl.get({
        callback: (result) => {
            res.json(result);
        }
    });
});

router.get('/users/find/:param', (req, res) => {
    UserCtrl.find({
        param: req.params.param,
        callback: (result) => {
            res.json(result);
        }
    });
});

router.get('/users/page/:page', (req, res) => {
    UserCtrl.get({
        page: parseInt(req.params.page),
        callback: (result) => {
            res.json(result);
        }
    });
});

router.get('/users/:id', (req, res) => {
    UserCtrl.get({
        id: req.params.id,
        callback: (result) => {
            res.json(result);
        }
    });
});

router.post('/user', (req, res) => {
    switch (req.body.procedure) {
        case 'OAUTH':
            {
                UserCtrl.create({
                    data: {
                        first_name: req.body.firstName,
                        last_name: req.body.lastName,
                        name: req.body.name,
                        email: req.body.email,
                        image_url: req.body.imageUrl
                    },
                    callback: (result) => {
                        res.json(result);
                    }
                });
                break;
            }
        default:
            {
                UserCtrl.authenticate({
                    data: {
                        user: req.body.user
                    },
                    callback: (result) => {
                        res.json(result);
                    }
                });
                break;        
            }
    }    
});

router.post('/user/register', (req, res) => {
    UserCtrl.create({
        data: {
            user: {
                first_name: req.body.firstName,
                last_name: req.body.lastName,
                username: req.body.username,
                email: req.body.email,
                password: req.body.password,
                userType:req.body.userType
            },
            reCaptcha: {
                value: req.body.reCaptcha
            }
        },
        callback: (result) => {
            res.json(result);
        }
    });
});

router.patch('/user', (req, res) => {
    UserCtrl.update({
        data: [
            {
                creator_id: req.body.creatorId,
                text: req.body.text,
                status: req.body.status
            }, {
                id: req.body.id
            }
        ],
        callback: (result) => {
            res.json(result);
        }
    });
});

router.delete('/user', (req, res) => {
    UserCtrl.destroy({
        data: {
            id: req.body.id
        },
        callback: (result) => {
            res.json(result);
        }
    });
});

module.exports = router;