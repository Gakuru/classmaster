const express = require('express');
const router = express.Router();
const rollcall = require('../controllers/rollcall.controller');
const appName = require('../utils/common').AppName;

router.get('/', (req, res) => {
  res.send(appName);
});

router.post('/rollcall/', (req, res) => {
  rollcall.saveRollcall({
    data: {
      teacher_id: req.body.teacherId,
      school_id: req.body.schoolId,
      unit_id: req.body.unitId,
      boys_absent: req.body.absBoys,
      girls_absent: req.body.absGirls,
      reasons: JSON.stringify(req.body.reasonRefs),
    },
    callback: (result) => {
      res.json(result);
    }
  });
});

module.exports = router;