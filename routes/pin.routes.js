const express = require('express');
const router = express.Router();
const pin = require('../controllers/pin.controller');
const appName = require('../utils/common').AppName;

router.get('/', (req, res) => {
  res.send(appName);
});

router.get('/pin/:phone/:id', (req, res) => {
  pin.get({
    data: {
      phone: req.params.phone,
      pin: req.params.id,
    },
    callback: (result) => {
      res.json(result);
    }
  });
});

module.exports = router;