const express = require('express');
const router = express.Router();
const appName = require('../utils/common').AppName;

const school = require('../models/schools.model');
const teacher = require('../models/pin.model');
const rollcall = require('../models/rollcall.model');

const redis = require("redis")
const redisClient = redis.createClient();

redisClient.on("error", (err) => {
  console.log("Error " + err);
});

router.get('/', (req, res) => {
  res.send(appName);
});

router.post('/callback/', (req, res) => {
  // Reads the variables sent via POST from our gateway
  const sessionId = req.body.sessionId;
  const serviceCode = req.body.serviceCode;
  const phoneNumber = req.body.phoneNumber;
  const text = req.body.text;

  let response = "";
  const splits = text.split('*');
  const level = splits.length - 1;

  const genders = [
    { ussdId: '1', name: 'Boys', key: 'boys' },
    { ussdId: '2', name: 'Girls', key: 'girls' },
  ]

  if (text == "" && level == 0) {
    // Get the school code
    response = "CON Welcome to classmaster, enter the school code \n";

    res.send(response);

  } else if (splits[0] && level == 0) {

    school.getByCode({ code: splits[0] }).then(result => {
      if (result.school) {
        // Request for a user pin
        response = `CON Please enter your pin for ${result.school.name} \n`;

      } else {
        response = "END A School with the code does not exist, please try again";
      }

      res.send(response);

    }).catch(err => {
      response = "END We couldn't complete your request at this time, please try again later";
      res.send(response);
    })

  } else if (level == 1) {
    teacher.get({ phone: phoneNumber, schoolCode: splits[0], pin: splits[1] }).then(result => {

      if (result.teacher) {

        //Persist the object in redis for approx ten minutes then mark the data as stale for deletion
        let _teacher = result.teacher;
        redisClient.set(sessionId, JSON.stringify(_teacher), redis.print);
        // Expire in 5 minutes
        redisClient.expire('string key', 300);

        response = "CON Select the class to take a rollcall for \n";

        if (_teacher.allocatedUnits.length) {
          _teacher.allocatedUnits.forEach(unit => {
            response += `${unit.ussdId} ${unit.type} ${unit.name}\n`
          });
        }

      } else {
        response = "END Invalid pin, please try again";
      }

      res.send(response);

    }).catch(err => {

      response = "END We couldn't complete your request at this time, please try again later";
      res.send(response);

    })
  } else if (level == 2) {
    if (splits[2]) {
      redisClient.get(sessionId, (err, result) => {
        if (err) throw err;

        let _teacher = JSON.parse(result);

        if (_teacher.allocatedUnits.length) {
          const unit = _teacher.allocatedUnits.filter(unit => { return unit.ussdId === splits[2] })[0];
          response = `CON Select the gender for ${unit.type} ${unit.name} \n`;
          genders.forEach(gender => {
            response += `${gender.ussdId} ${gender.name} Absent\n`
          });
          res.send(response);
        }

      });

    } else {
      response = "END response cannot be empty \n";
      res.send(response);
    }

  } else if (level > 2) {
    if (splits[3]) {

      redisClient.get(sessionId, (err, result) => {
        if (err) throw err;

        let _teacher = JSON.parse(result);

        if (_teacher.reasons.length && _teacher.reasons.length) {
          const unit = _teacher.allocatedUnits.filter(unit => { return unit.ussdId === splits[2] })[0];

          const selectedGender = genders.filter(gender => { return gender.ussdId === splits[3] })[0];

          const reasonSplits = splits.splice(4);
          const reasonLevel = reasonSplits.length;

          const reason = _teacher.reasons[reasonLevel];

          if (reason) {
            if (reasonLevel) {
              _teacher.reasons[reasonLevel - 1].absenseCount[selectedGender.key] = parseInt(reasonSplits[reasonLevel - 1]);
              redisClient.set(sessionId, JSON.stringify(_teacher), redis.print);
            }

            response = `CON Enter ${unit.type} ${unit.name} ${selectedGender.name} absent due to ${reason.name}\n`;
            res.send(response);
          } else {
            // No more reasons to loop through - persist the data
            const allReasonsFilled = _teacher.reasons.filter(reason => { return reason.absenseCount[selectedGender] !== null });

            if (allReasonsFilled.length) {
              if (reasonLevel <= allReasonsFilled.length) {
                _teacher.reasons[reasonLevel - 1].absenseCount[selectedGender.key] = parseInt(reasonSplits[reasonLevel - 1]);
                redisClient.set(sessionId, JSON.stringify(_teacher), redis.print);

                response = `CON submit ${unit.type} ${unit.name} ${selectedGender.name} rollcall \n 1 Accept \n 2 Decline \n`;
                res.send(response);
              } else {
                const acceptSplits = reasonSplits.splice(allReasonsFilled.length);
                if (acceptSplits[0] === '1') {
                  //Persist the data

                  const data = {
                    teacher_id: _teacher.teacherId,
                    school_id: _teacher.schoolId,
                    unit_id: _teacher.allocatedUnits.filter(unit => { return unit.ussdId === splits[2] })[0].id,
                    genderDiscriminator: selectedGender.key,
                    total_absent: _teacher.reasons.reduce((a, b) => { return a + b.absenseCount[selectedGender.key] }, 0),
                    selectedGender,
                    reasons: _teacher.reasons
                  }

                  rollcall.saveRollcall(data).then(result => {
                    response = `END ${unit.type} ${unit.name} ${selectedGender.name} results submitted \n`;
                    res.send(response);
                  }).catch(err => {
                    console.log(err);
                  })

                } else {
                  //Submission cancelled by the user
                  response = `END \n`;
                  res.send(response);
                }

              }
            } else {
              _teacher.reasons[reasonLevel - 1].absenseCount[selectedGender.key] = parseInt(reasonSplits[reasonLevel - 1]);
              redisClient.set(sessionId, JSON.stringify(_teacher), redis.print);
            }

          }

        } else {
          response = "END You have no allocated classes to take a rollcall for \n";
          res.send(response);
        }

      });

    } else {
      response = "END response cannot be empty \n";
      res.send(response);
    }
  }
});

module.exports = router;