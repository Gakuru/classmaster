module.exports = {
    generateQrCode: (text) => {
        const qr = require('qr-image');
        const fs = require('fs');
        const moment = require('moment');
        
        const type = 'png';
        const qrCode = qr.image(text, { type });
        const name = `${text}_${moment().format('YYYY_MM_DD_HH_mm_ss')}.${type}`;
        const path = `/var/www/html/qrcodes/${name}`;
        const output = fs.createWriteStream(path);
        qrCode.pipe(output);
        return name;
    }
}