const connection = require('../config/db/mysql');
const error = require('../errors/errors');

const moment = require('moment');

const appUser = require('../auth/user');

module.exports = {
  get: (period, callback) => {
    var _result = {
      consumptionByFloor: [],
      consumptionByWing: [],
      consumptionByUnit: []
    };
    connection.query('SELECT wing,SUM(consumption) as consumption FROM `vtenatUtilityReadings` WHERE `' +
        'pid` = ? AND `forMonth` = ? GROUP BY wing',
    [
      appUser.user.pid, period
    ], (err, result, fields) => {
      if (err) 
        throw err;
      _result.consumptionByWing = result;
      connection.query('SELECT floor,SUM(consumption) as consumption FROM `vtenatUtilityReadings` WHERE ' +
          '`pid` = ? AND `forMonth` = ? GROUP BY floor',
      [
        appUser.user.pid, period
      ], (err, result, fields) => {
        if (err) 
          throw err;
        _result.consumptionByFloor = result;
        connection.query('SELECT houseNo,SUM(consumption) as consumption FROM `vtenatUtilityReadings` WHER' +
            'E `pid` = ? AND `forMonth` = ? GROUP BY houseNo',
        [
          appUser.user.pid, period
        ], (err, result, fields) => {
          if (err) 
            throw err;
          _result.consumptionByUnit = result;
          callback(_result);
        });
      });
    });
  },
  getFinancialSummary: (callback) => {

    var _result = {
      unpaidRent: {
        thisMonth: {
          tag: 'thisMonth',
          period: moment().format('MMMM YYYY'),
          count: 0,
          total: 0,
          value: 0
        },
        lastMonth: {
          tag: 'lastMonth',
          period: moment().subtract(1,'months').format('MMMM YYYY'),
          count: 0,
          total: 0,
          value: 0
        },
        thisYear: {
          tag: 'thisYear',
          period: moment().format('YYYY'),
          count: 0,
          total: 0,
          value: 0
        }
      },
      unpaidUtilities: {
        thisMonth: {
          tag: 'thisMonth',
          period: moment().format('MMMM YYYY'),
          count: 0,
          total: 0,
          value: 0
        },
        lastMonth: {
          tag: 'lastMonth',
          period: moment().subtract(1,'months').format('MMMM YYYY'),
          count: 0,
          total: 0,
          value: 0
        },
        thisYear: {
          tag: 'thisYear',
          period: moment().format('YYYY'),
          count: 0,
          total: 0,
          value: 0
        }
      },
      unpaidInvoices: {
        thisMonth: {
          tag: 'thisMonth',
          period: moment().format('MMMM YYYY'),
          count: 0,
          total: 0,
          value: 0
        },
        lastMonth: {
          tag: 'lastMonth',
          period: moment().subtract(1,'months').format('MMMM YYYY'),
          count: 0,
          total: 0,
          value: 0
        },
        thisYear: {
          tag: 'thisYear',
          period: moment().format('YYYY'),
          count: 0,
          total: 0,
          value: 0
        }
      }
    }

    connection.query("CALL unpaid_rent_summary (?);", [appUser.user.pid], (err, results, fields) => {
      if (err) 
        throw err;
      
      if (results[0].length) {
        var results = results[0];

        let a = _result.unpaidRent.thisMonth;
        let b = _result.unpaidRent.lastMonth;
        let c = _result.unpaidRent.thisYear;

        if(results[0]){
          a.tag = results[0].tag;
          a.period = results[0].forMonth;
          a.count = results[0].unpaidCount;
          a.total = results[0].unpaidTotal;
          a.value = results[0].unpaidValue;
        }

        if(results[1]){
          b.tag = results[1].tag;
          b.period = results[1].forMonth;
          b.count = results[1].unpaidCount;
          b.total = results[1].unpaidTotal;
          b.value = results[1].unpaidValue;
        }

        if(results[2]){
          c.tag = results[2].tag;
          c.period = results[2].forMonth;
          c.count = results[2].unpaidCount;
          c.total = results[2].unpaidTotal;
          c.value = results[2].unpaidValue;
        }

      }

      connection.query("CALL unpaid_utilities_summary (?);", [appUser.user.pid], (err, results, fields) => {
        if (err) 
          throw err;
        if (results[0].length) {
          var results = results[0];

          let a = _result.unpaidUtilities.thisMonth;
          let b = _result.unpaidUtilities.lastMonth;
          let c = _result.unpaidUtilities.thisYear;

          if(results[0]){
            a.tag = results[0].tag;
            a.period = results[0].forMonth;
            a.count = results[0].unpaidCount;
            a.total = results[0].unpaidTotal;
            a.value = results[0].unpaidValue;
          }
  
          if(results[1]){
            b.tag = results[1].tag;
            b.period = results[1].forMonth;
            b.count = results[1].unpaidCount;
            b.total = results[1].unpaidTotal;
            b.value = results[1].unpaidValue;
          }
  
          if(results[2]){
            c.tag = results[2].tag;
            c.period = results[2].forMonth;
            c.count = results[2].unpaidCount;
            c.total = results[2].unpaidTotal;
            c.value = results[2].unpaidValue;
          }

        }

        connection.query("CALL unpaid_invoices_summary (?);", [appUser.user.pid], (err, results, fields) => {
          if (err) 
            throw err;
          if (results[0].length) {
            var results = results[0];

            let a = _result.unpaidInvoices.thisMonth;
            let b = _result.unpaidInvoices.lastMonth;
            let c = _result.unpaidInvoices.thisYear;

            if(results[0]){
              a.tag = results[0].tag;
              a.period = results[0].forMonth;
              a.count = results[0].unpaidCount;
              a.total = results[0].unpaidTotal;
              a.value = results[0].unpaidValue;
            }
    
            if(results[1]){
              b.tag = results[1].tag;
              b.period = results[1].forMonth;
              b.count = results[1].unpaidCount;
              b.total = results[1].unpaidTotal;
              b.value = results[1].unpaidValue;
            }
    
            if(results[2]){
              c.tag = results[2].tag;
              c.period = results[2].forMonth;
              c.count = results[2].unpaidCount;
              c.total = results[2].unpaidTotal;
              c.value = results[2].unpaidValue;
            }

            callback(_result);
          }

        });

      });

    });
  }
};
