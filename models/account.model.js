const connection = require('../config/db/mysql');

const uuid = require('uuid/v1');

const urls = require('../config/urls/urls');
const queryLimit = require('../config/pager/pager');
const error = require('../errors/errors');
const pager = require('../pager/pager');
const transformer = require('../transformers/transformer');

const appUser = require('../auth/user');

const DB = require('./db');

const commands = require('../config/commands/commands');

const account = module.exports = {    
    getNonificationSettings: ({ category = undefined }) => {
        return new Promise((resolve, reject) => {
            DB.exec('CALL `get_notification_settings`(?)', [appUser.user.pid]).then((settings) => {
                return resolve({
                    code: 200,
                    success: true,
                    message: 'Get notification settings success',
                    settings: {
                        notificationSettings: transformer.transformNotificationSettings(settings)
                    }
                });
            }).catch((err) => {
                throw err;
                return resolve({
                    code: 400,
                    success: true,
                    message: 'Get notification settings failed',
                    settings: {
                        notificationSettings: err
                    }
                });
            });
        });
    },
    getAccountBalabce: () => {
        return new Promise((resolve, reject) => {
            DB.exec('CALL `get_account_balance`(?)', [appUser.user.pid]).then((account) => {
                return resolve({
                    code: 200,
                    success: true,
                    message: 'Get account balance success',
                    account: {
                        account: transformer.transformAccountBalance(account)
                    }
                });
            }).catch((err) => {
                return resolve({
                    code: 400,
                    success: true,
                    message: 'Get account balance failed',
                    settings: {
                        notificationSettings: err
                    }
                });
            });
        });
    },
    saveNotificationSettings: ({ data, transform = true, command = commands.SAVE }) => {
        return new Promise((resolve, reject) => {
            data.id = uuid();
            DB.exec('CALL `insert_notification_setting`(?,?,?,?,?);', [data.id, appUser.user.pid, data.category, data.settingKey, data[data.settingKey]]).then((result) => {
                return resolve({
                    code: 200,
                    success: true,
                    message: 'Save notification setting success',
                    settings: {
                        smsNotificationSettings: [data]
                    }
                });
            }).catch((err) => {
                throw err;
                return reject({code: 404, success: false, message: 'Save notification setting failed', settings: err});
            });
        });
    }
}
