const CONNECTION = require('../config/db/mysql');

const DB = module.exports = {
  raw: (query, params, connection = CONNECTION) => {
    return new Promise((resolve, reject) => {
      connection.query(query, params, (err, result, fields) => {
        if (err) return reject(err);
        return resolve(result);
      });
    });
  },
  exec: (query, params, connection = CONNECTION) => {
    return new Promise((resolve, reject) => {
      connection.query(query, params, (err, result, fields) => {
        if (err) return reject(err);
        return resolve(result[0]);
      });
    });
  },
  begin: () => {
    return new Promise((resolve, reject) => {
      CONNECTION.getConnection((err, connection) => {
        if (err) return reject(err);
        return resolve(connection);
      });
    });
  },
  commit: (connection) => {
    return new Promise((resolve, reject) => {
      connection.commit((err) => {
        if (err)
          return connection.rollback(() => {
            return reject(err);
          });

        connection.release();

        return resolve(true);

      });

    });
  },
  release: (connection) => {
    connection.release();
  }
}