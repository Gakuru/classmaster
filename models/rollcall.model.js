
const uuid = require('uuid/v4');
const moment = require('moment');

const urls = require('../config/urls/urls');
const pager = require('../pager/pager');
const transformer = require('../transformers/transformer');

const appUser = require('../auth/user');

const DB = require('./db');

const teacher = module.exports = {
    saveRollcall: (data) => {
        return new Promise((resolve, reject) => {

            data.reasons.forEach((reason, i) => {

                const attendanceData = {
                    id: uuid(),
                    school_id: data.school_id,
                    teacher_id: data.school_id,
                    unit_id: data.unit_id,
                    reason_id: reason.id,
                    posting_date: moment().format('YYYY-MM-DD'),
                    gender_discriminator: data.genderDiscriminator,
                    absense_count: reason.absenseCount[data.genderDiscriminator]
                }

                DB.raw('INSERT INTO `attendance` SET ?', attendanceData).then((result) => {
                    if ((data.reasons.length - 1) === i) {
                        return resolve({
                            code: 200,
                            success: true,
                            message: 'Save rollcall success',
                            rollcall: data
                        });
                    }
                }).catch((err) => {
                    return reject({
                        code: 404,
                        success: false,
                        message: 'Save rollcall failed',
                        rollcall: err
                    });
                });
            });
        });
    },
}
