const connection = require('../config/db/mysql');

const DB = require('./db');

const commands = require('../config/commands/commands');

const uuid = require('uuid/v4');

const jwt = require('jsonwebtoken');

const _ = require('lodash');

const nodemailer = require('nodemailer');

const buildQuery = (command) => ({
    [commands.SAVE]: { parent: 'CALL `insert_user` (?,?,?,?,?,?,?);' }
})[command];

const dataConfig = (data, command) => {
    return {
        [commands.SAVE]: () => {
            data.id = uuid();
            // data.pid = uuid();
            data.pid = 1;
            return [
                data.id,
                data.pid,
                data.first_name,
                data.last_name,
                data.name,
                data.email,
                data.image_url
            ];
        }
    }[command]();
};

function* queryConfig(data, command) {
    yield buildQuery(command);
    yield dataConfig(data, command);
}

const signedToken = (data) => {
    return jwt.sign({
        user: {
            id: data.id,
            pid: data.pid,
            sid: data.sid,
            email: data.email,
            userFirstName: data.userFirstName,
            name: data.name,
            accout: data.account
        },
        // exp: Math.floor(Date.now() / 1000) + ((60 * 4) * 60) //Expires in four hours
    }, 'secret');
}

/** Todo Refactor to a Generic */
const sendWelcomeMail = (data) => {
    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: process.env.GMAIL_USERNAME,
            pass: process.env.GMAIL_PASSWORD
        }
    });

    const mailOptions = {
        from: 'Oak <renter.co.ke@gmail.com>',
        to: data.email,
        subject: 'Welcome to Oak',
        html: `<div>
        <div>
    We are deligted to see you here. Oak facilites renting by automating and simplyfying virtualay all renting tasks
</div>
<div>
    They use <a href="https://oak.co.ke">Oak</a> to simplyfy their operations
</div>
<div>
Cheers,
Oak Team
<hr />
Support
<p>
    <a href="mailto:info.renter.co.ke@gmail.com?Subject=Support" target="_top">Send Mail</a>
</p>
Or
<p>
    Write to us at info.renter.co.ke@gmail.com or renter.co.ke@gmail.com
</p>
</div>
        </div>`
    };

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
}

const verifyReCaptcha = async (reCaptcha, secret = '6Ld87XQUAAAAABaV6nXWbel94SWldbcJXm-vBdDX') => {
    const axios = require('axios');
    const response = await axios.post(`https://www.google.com/recaptcha/api/siteverify?secret=${secret}&response=${reCaptcha}`);
    return response.data;
}

const constructVerificationCode = () => {
    const randomstring = require('randomstring');
    return randomstring.generate({
        length: 7,
        charset: 'alphanumeric',
        capitalization: 'uppercase'
    });
};

const User = module.exports = {
    User: {
        id: undefined,
        pid: undefined,
        first_name: undefined,
        last_name: undefined,
        name: undefined,
        email: undefined,
        image_url: undefined
    },
    get: (id) => {
        return new Promise((resolve, reject) => {
            let response = {
                status: 200,
                success: true,
                action: 'Get User',
                message: 'Users retrieved successfuly',
                users: {}
            };

            connection.query('CALL `get_users`;', (err, results, fields) => {
                if (err) {
                    response.message = 'Users could not be retrieved';
                    return reject(response);
                } else {
                    response.users = results[0];
                    return resolve(response);
                }
            });
        });
    },
    find: (param) => {
        return new Promise((resolve, reject) => {
            let response = {
                status: 200,
                success: true,
                action: 'Find User',
                message: 'Users retrieved successfuly',
                users: {}
            };

            connection.query('CALL `find_user`(?);', [param], (err, results, fields) => {
                if (err) {
                    response.message = 'Users could not be retrieved';
                    return reject(response);
                } else {
                    response.users = results[0];
                    return resolve(response);
                }
            });
        });
    },
    save: (data, command) => {
        return new Promise((resolve, reject) => {
            verifyReCaptcha(data.reCaptcha.value).then(response => {
                if (response.success) {
                    let { user } = data;
                    user.vericationCode = constructVerificationCode();
                    connection.getConnection((err, connection) => {
                        user.pid = uuid();
                        user.sid = uuid();
                        connection.beginTransaction((err) => {
                            connection.query('CALL `insert_parent` (?,?,?,?,?,?);', [user.pid, user.username, `${user.first_name} ${user.last_name}`, user.vericationCode, user.email, user.userType], (err, results, fields) => {
                                if (err) throw err;
                                user.id = uuid();
                                connection.query('CALL `insert_user` (?,?,?,?,?,?,?,?,?,?,?);', [user.id, user.pid, user.sid, user.first_name, user.last_name, `${user.first_name} ${user.last_name}`, user.email, user.username, user.password, null, user.userType], (err, results, fields) => {
                                    if (err) throw err;
                                    sendWelcomeMail(user);
                                    return resolve({
                                        status: 200,
                                        success: true,
                                        action: 'Auth User',
                                        message: 'User registered successfuly',
                                        user: {
                                            user: {
                                                id: user.id,
                                                pid: user.pid,
                                                sid: user.sid,
                                                email: user.email,
                                                userFirstName: user.first_name,
                                                userType: user.userType,
                                                account: user.username,
                                                name: `${user.first_name} ${user.last_name}`,
                                            },
                                            token: signedToken({
                                                id: user.id,
                                                pid: user.pid,
                                                sid: user.sid,
                                                email: user.email,
                                                userFirstName: user.first_name,
                                                userType: user.userType,
                                                account: user.username,
                                                name: `${user.first_name} ${user.last_name}`,
                                            })
                                        }
                                    });
                                });
                            });

                            // Commit
                            connection.commit((err) => {
                                if (err) {
                                    return connection.rollback(() => {
                                        return reject({
                                            code: 404,
                                            success: false,
                                            message: 'Register user failed',
                                            user: {
                                                data: err
                                            }
                                        });
                                    });
                                } else {
                                    User.authenticate({ user: { username: user.email, password: user.password } }).then(result => {
                                        return resolve(result);
                                    });
                                }
                            });
                        });
                    });
                }
            });

            return;

            let query = queryConfig(data, command);

            connection.query(query.next().value, query.next().value, (err, results, fields) => {
                if (err)
                    return reject({
                        status: 400,
                        success: false,
                        action: 'Save User',
                        command: command,
                        message: 'Saving task failed',
                        err: err
                    });

                if (results.affectedRows === 1) {
                    sendWelcomeMail(data);
                }

                if (results.affectedRows === 0) {
                    connection.query('SELECT `id`,`pid` FROM `users` WHERE ?', {
                        email: data.email
                    }, (err, results, fields) => {
                        data.id = results[0].id;
                        data.pid = results[0].pid;

                        return resolve({
                            status: 200,
                            success: true,
                            action: 'Auth User',
                            command: command,
                            message: 'User Oauthed successfuly',
                            user: {
                                user: data,
                                token: signedToken(data)
                            }
                        });
                    });
                } else {

                    return resolve({
                        status: 200,
                        success: true,
                        action: 'Auth User',
                        command: command,
                        message: 'User Oauthed successfuly',
                        user: {
                            user: data,
                            token: signedToken(data)
                        }
                    });
                }

            });
        });
    },
    authenticate: ({ user }) => {
        return new Promise((resolve, reject) => {
            verifyReCaptcha(user.reCaptcha, '6Lc4CHUUAAAAACArzHSUM5QJOantv1hPS5GGxcob').then(response => {
                if (response.success) {
                    DB.exec('CALL `authenticate_user`(?,?);', [user.username, user.password]).then((result) => {
                        user = _.first(result);
                        return resolve({
                            status: 200,
                            success: true,
                            action: 'Auth User',
                            message: 'User Oauthed successfuly',
                            user: {
                                user: user,
                                token: signedToken(user)
                            }
                        });
                    }).catch((err) => {
                        throw err
                        return reject({
                            status: 400,
                            success: false,
                            action: 'Auth User',
                            message: 'User auth failed',
                            err: err
                        });
                    });
                }
            });
        });
    },
    remove: (data, command) => {
        return new Promise((resolve, reject) => {
            let a = true;
            if (a) {
                return resolve({ status: 200, success: true, action: 'Remove User', message: 'User removed successfuly' });
            } else {
                return reject({ status: 400, success: false, action: 'Remove User', message: 'User removal failed' });
            }
        });
    }
};