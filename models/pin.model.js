const DB = require('./db');

const teacher = module.exports = {
    get: ({ phone, pin, schoolCode }) => {
        return new Promise((resolve, reject) => {
            DB.exec('CALL `auth_user`(?,?,?)', [phone, pin, schoolCode]).then((result) => {
                return resolve({
                    code: 200,
                    success: true,
                    message: 'Get teacher success',
                    teacher: result.map(result => {
                        return {
                            teacherId: result.teacherId,
                            teacherNames: result.teacherNames,
                            schoolId: result.schoolId,
                            schoolCode: result.schoolCode,
                            schoolName: result.schoolName,
                            allocatedUnits: JSON.parse(result.allocatedUnits).map((unit, i) => {
                                return {
                                    ussdId: (++i).toString(),
                                    id: unit.id,
                                    name: unit.name,
                                    type: unit.type,
                                    responsibility: unit.responsibility
                                }
                            }),
                            reasons: JSON.parse(result.reasons).map((reason, i) => {
                                return {
                                    ussdId: (++i).toString(),
                                    id: reason.id,
                                    name: reason.reason,
                                    genderOrientation: reason.genderOrientation,
                                    absenseCount: {
                                        girls: null,
                                        boys: null,
                                    }
                                }
                            })
                        }
                    })[0]
                });
            }).catch((err) => {
                return reject({
                    code: 404,
                    success: false,
                    message: 'Get teacher failed',
                    teacher: err
                });
            });
        });
    },
}
