const DB = require('./db');

const schools = module.exports = {
    getByCode: ({ code }) => {
        return new Promise((resolve, reject) => {
            console.log(code);
            DB.exec('CALL `get_school`(?)', [code]).then((result) => {
                return resolve({
                    code: 200,
                    success: true,
                    message: 'Get schools success',
                    school: result.map(school => {
                        return {
                            code: school.schoolCode,
                            name: school.schoolName
                        }
                    })[0]
                });
            }).catch((err) => {
                return reject({ code: 404, success: false, message: 'Get schools failed', schools: err });
            });
        });
    },
}
