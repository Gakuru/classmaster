
const uuid = require('uuid/v4');

const urls = require('../config/urls/urls');
const pager = require('../pager/pager');
const transformer = require('../transformers/transformer');

const appUser = require('../auth/user');

const DB = require('./db');

const commands = require('../config/commands/commands');

const createPager = (result, nextStart, searchTerm) => {
    let popped = (result.length > pager.options.limit);
    if (popped)
        result.pop();
    return {
        pager: pager.getPager(popped, nextStart, !searchTerm
            ? urls.pumpsURL
            : `${urls.pumpsURL}/search/${searchTerm}`),
        data: transformer.transformPumps(result)
    };
};

const buildQuery = (command) => ({
    [commands.GET]: 'CALL `get_pumps` (?,?,?,?);',
    [commands.GET_BY_ID]: 'CALL `get_pumps` (?);',
    [commands.FIND]: 'CALL `find_pumps` (?,?,?,?,?);',
    [commands.SAVE]: 'INSERT INTO `pumps` SET ?;',
    [commands.UPDATE]: 'UPDATE `pumps` SET ? WHERE ?;'
})[command];

const dataConfig = (data, command) => {
    return {
        [commands.GET]: () => {
            return data;
        },
        [commands.GET_BY_ID]: () => {
            return data;
        },
        [commands.FIND]: () => {
            return data;
        },
        [commands.SAVE]: () => {
            data.id = uuid();
            data.pid = appUser.user.pid;
            data.sid = appUser.user.sid;
            let nozzles = data.nozzles.map(nozzle => {
                return {
                    id: uuid(),
                    pid: data.pid,
                    sid: data.sid,
                    pumpId: data.id,
                    name: nozzle.name,
                    initialReading: nozzle.initialReading,
                    fuelTypeId: nozzle.fuelTypeId,
                    fuelTypeName: nozzle.fuelTypeName,
                }
            })

            delete data.nozzles;

            return { data, nozzles };
        },
        [commands.UPDATE]: () => {
            return data;
        }
    }[command]();
};

function* queryConfig(data, command) {
    yield buildQuery(command);
    yield dataConfig(data, command);
}

const pumps = module.exports = {
    get: ({ page = 1, id = undefined, command = commands.GET }) => {
        return new Promise((resolve, reject) => {
            let offset = ((page * pager.options.limit) - pager.options.limit);
            let data = id ? [id] : [appUser.user.pid, appUser.user.sid, offset, (pager.options.limit + 1)];

            let query = queryConfig(data, command);

            DB.exec(query.next().value, query.next().value).then((result) => {
                return resolve({
                    code: 200,
                    success: true,
                    message: 'Get pumps success',
                    pumps: createPager(result, page)
                });
            }).catch((err) => {
                return reject({ code: 404, success: false, message: 'Get pumps failed', pumps: err });
            });
        });
    },
    getNozzle: ({ id }) => {
        return new Promise((resolve, reject) => {
            DB.exec('CALL get_nozzle(?,?,?)', [appUser.user.pid, appUser.user.sid, id]).then((result) => {
                return resolve({
                    code: 200,
                    success: true,
                    message: 'Get nozzle success',
                    nozzle: result.map(_nozzle => {
                        const nozzle = JSON.parse(_nozzle.nozzle);
                        return {
                            pumpId: _nozzle.pumpId,
                            pumName: _nozzle.pumpName,
                            nozzle: {
                                id: nozzle.id,
                                name: nozzle.name,
                                fuelType: {
                                    id: nozzle.fuelTypeId,
                                    name: nozzle.fuelTypeName,
                                },
                                readings: {
                                    current: _nozzle.current_reading,
                                    previous: _nozzle.previous_reading,
                                }
                            }
                        }
                    })
                });
            }).catch((err) => {
                return reject({ code: 404, success: false, message: 'Get nozzle failed', nozzle: err });
            });
        });
    },
    find: ({ param, page = 1, command = commands.FIND }) => {
        return new Promise((resolve, reject) => {

            let offset = ((page * pager.options.limit) - pager.options.limit);
            let data = [appUser.user.pid, param, offset, (pager.options.limit + 1)];
            let query = queryConfig(data, command);

            DB.exec(query.next().value, query.next().value).then((result) => {
                return resolve({
                    code: 200,
                    success: true,
                    message: 'Get pumps success',
                    pumps: createPager(result, page, param)
                });
            }).catch((err) => {
                return reject({ code: 404, success: false, message: 'Get pumps failed', pumps: err });
            });

        });
    },
    save: ({ data, command = commands.SAVE }) => {
        return new Promise((resolve, reject) => {
            if (command === commands.SAVE) {
                let query = queryConfig(data, command);
                const _query = query.next().value;
                const _data = query.next().value;
                DB.raw(_query, _data.data).then((result) => {
                    _data.nozzles.forEach(nozzle => {
                        DB.raw('INSERT INTO pump_nozzles SET ?', nozzle).then(result => {
                        }).catch(err => {
                            throw err;
                        })
                    });
                    return resolve({
                        code: 200,
                        success: true,
                        message: 'Save pump success',
                        pumps: {
                            data: transformer.transformPumps([data])
                        }
                    });
                }).catch((err) => {
                    return reject({ code: 404, success: false, message: 'Save pump failed', pumps: err });
                });
            } else {

                let nozzles = data.nozzles.map(nozzle => {
                    return {
                        id: nozzle.id,
                        pid: appUser.user.pid,
                        sid: appUser.user.sid,
                        pumpId: data.id,
                        fuelTypeId: nozzle.fuelTypeId,
                        fuelTypeName: nozzle.fuelTypeName,
                        name: nozzle.name,
                        initialReading: nozzle.initialReading,
                    }
                });

                data.nozzles = nozzles;

                DB.raw('update pumps set ? where ?', [{ name: data.name }, { id: data.id }]).then(result => {
                    if (result.affectedRows) {
                        nozzles.forEach((nozzle, i) => {
                            DB.raw('call insert_nozzle(?,?,?,?,?,?,?,?);', [nozzle.id, nozzle.pid, nozzle.sid, nozzle.pumpId, nozzle.fuelTypeId, nozzle.fuelTypeName, nozzle.name, nozzle.initialReading]).then(result => {
                                if (i === nozzles.length - 1) {
                                    return resolve({
                                        code: 200,
                                        success: true,
                                        message: 'Save pump success',
                                        pumps: data
                                    });
                                }
                            }).catch(err => {
                                throw err;
                            })
                        });
                    }
                }).catch(err => {
                    throw err
                    // return reject({ code: 404, success: false, message: 'Save pump failed', pumps: err });
                });
            }
        });
    },
    remove: ({ data, command = commands.UPDATE }) => {
        return new Promise((resolve, reject) => {
            let query = queryConfig(data, command);
            DB.raw(query.next().value, query.next().value).then((result) => {
                pumps.get({}).then((pumps) => {
                    return resolve({ code: 200, success: true, message: 'Remove pumps success', pumps: pumps.pumps });
                }).catch((err) => {
                    return reject(err);
                });
            }).catch((err) => {
                return reject({ code: 404, success: false, message: 'Remove pumps failed', pumps: err });
            });
        });
    },
    removeNozzle: ({ data }) => {
        return new Promise((resolve, reject) => {
            DB.raw('UPDATE `pump_nozzles` SET ? WHERE ?', data).then((result) => {
                return resolve({ code: 200, success: true, message: 'Remove pump nozzles success', pumpNozzles: [] });
            }).catch((err) => {
                return reject({ code: 404, success: false, message: 'Remove pump nozzles failed', pumpNozzles: err });
            });
        });
    }
}
