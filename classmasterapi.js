const express = require('express');
const app = express();
const ports = [2080, 2081];
const path = require('path');
const dotenv = require('dotenv').config({ path: path.join(__dirname, '.env') });
const bodyParser = require('body-parser');


const pin = require('./routes/pin.routes');
const rollcall = require('./routes/rollcall.routes');
const callback = require('./routes/callback.routes');

const jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens

const auth = require('./auth/auth');

app.set('trust proxy', 'loopback');

app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(bodyParser.json());

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,x-access-token');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

app.use((req, res, next) => {
  if (req.headers['x-access-token']) {
    jwt
      .verify(req.headers['x-access-token'], 'secret', function (err, decode) {
        if (err)
          req.token = undefined;
        req.token = decode;
        next();
      });
  } else {
    req.token = undefined;
    next();
  }
});

app.use('/', pin);
app.use('/', rollcall);
app.use('/', callback);

ports.forEach((port, i) => {
  app.listen(port, () => {
    console.log('manage api listening on port %s', port);
  });
});